PINECON CONSOLE APPLICATION

Memory like a sieve? Now there’s no need to remember all 
                                those things that you have to do, because Pinecon will do that for you! 
                                The application will help you to remind both about important events (birthdays, holidays) 
                                and about daily routine. You can create simple tasks and plans, group them 
                                and give them to other users for execution.

__Installation__   

1. Download pinecon package
    * [Wheel](https://yadi.sk/d/cBPOSiKZ3aW8VD)
    * [Archive](https://yadi.sk/d/AjNAIUYn3aW8Vf)
2. In your terminal print
    * ``` pip install <path to download file> ``` 
3. You can work with pinecon :)

__Configurations__


```
{
    "level": "DEBUG",
    "format": "[%(levelname)s] %(asctime)s - %(name)s: %(message)s",
    "path": "/home/dubhad/.pinecon/pinelog.log"
}
```

__Usage__
   
```pine lodin -u ligonberry```    
```pine task add -t 'Feed cat'```    
```pine group add -t 'Work'```

_More commands_:

```pine -h```

__Installation API__ 

1. Download pinecon API package
    * [Wheel](https://yadi.sk/d/8CpIEi3-3aWL5F)
    * [Archive](https://yadi.sk/d/9d4L0T0P3aWL6n)
2. In your terminal print
    * ``` pip install <path to download file> ``` 
3. You can work with pinecon API :)
