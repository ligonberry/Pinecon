"""
    This package offers all the necessary classes and functions for working with pinecon library.

    Packages:
    ----------------
        models - provides modules, to represent abstractions for the pinecon library;
        services - provides modules to work with library at a higher level (with exception handling);
        storage - provides modules to work with library at a low level (databases and queries to it);

"""
