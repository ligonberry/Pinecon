""" This package provides modules, to represent abstractions for the pinecon library.

    Modules:
    ----------------
        helpers - helps you work with the options, that can take class attributes;
        models - contains data models as they are stored in database;
        views - contains extended data models for the view;

"""