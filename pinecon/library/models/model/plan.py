from enum import Enum

from pinecon.library.models import helpers


class PlanRepetitionStyle(Enum):
    YEARLY = "yearly"
    MONTHLY = "monthly"
    WEEKLY = "weekly"
    DAILY = "daily"


PLAN_REPETITION_STYLE_TUPLE = ("yearly", "monthly", "weekly", "daily")


class PlanEndReason(Enum):
    NEVER = "never"
    AFTER_NUMBER_OF_TIMES = "number"
    AFTER_DATE = "date"


PLAN_END_REASON_TUPLE = ("never", "number", "date")


class Plan:
    """ Model for method of achieving something that you have worked out in detail beforehand. """
    def __init__(self, start_date, repeat_style, interval_counter , end_reason, **kwargs):
        self.start_date = start_date
        self.repeat_style = repeat_style
        self.interval_counter = interval_counter
        self.end_reason = end_reason

        prop_defaults = {
            "plan_id": None,
            "end_date": None,
            "count": None,  # Number of repetitions until the task is finished
            "task_id": None,
            "counter": 0,  # Internal counter
            "active": helpers.YES
        }

        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))


class PlanAttribute(Enum):
    START_DATE = "start_date"
    REPEAT_STYLE = "repeat_style"
    INTERVAL_COUNTER = "interval_counter"
    END_REASON = "end_reason"

    ID = "plan_id"
    END_DATE = "end_date"
    COUNT = "count"  # Number of repetitions until the task is finished.
    TASK_ID = "task_id"
    ACTIVE = "active"