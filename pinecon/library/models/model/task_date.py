from enum import Enum


class TaskDateType(Enum):
    START = "start"
    END = "end"
    EVENT = "event"


TASK_DATE_TYPE_TUPLE = ("start", "end", "event")


class TaskDate:
    """ Model for a specific time that can be named, for example a particular day or a particular year. """

    def __init__(self, task_id, date_type, date, date_id=None):
        self.task_id = task_id
        self.date_type = date_type
        self.date = date

        self.date_id = date_id


class TaskDateAttribute(Enum):
    TASK_ID = "task_id"
    TYPE = "date_type"
    DATE = "date"

    DATE_ID = "date_id"
