from enum import Enum


class TaskRelation(Enum):
    BLOCKING = "blocking"
    DEPENDS_ON = "depends"
    CONNECTED = "connected"


TASK_RELATIONS_TUPLE = ("blocking", "depends", "connected")


class TaskToTaskRelation:
    """ Model for interaction between two tasks. """
    def __init__(self, parent_task_id, subtask_id, **kwargs):
        self.parent_task_id = parent_task_id
        self.subtask_id = subtask_id

        prop_defaults = {
            "relation_id": None,
            "relation_type": TaskRelation.CONNECTED.value
        }

        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))


class TaskToTaskRelationAttribute(Enum):
    PARENT_TASK_ID = "parent_task_id"
    SUBTASK_ID = "subtask_id"

    RELATION_ID = "relation_id"
    RELATION_TYPE = "relation_type"
