from enum import Enum


class User:
    """ Model for a person who uses this application. """

    def __init__(self, name, user_id=None, **kwargs):
        self.name = name
        self.user_id = user_id


class UserAttribute(Enum):
    NAME = "name"

    ID = "user_id"
