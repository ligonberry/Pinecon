# coding=utf-8
""" This module contains extended data models for the representation.

    Classes:
    ----------------
        * TaskVRepresentation - represents Task model;
        * TasksRepresentation - represents set of Task models;
        * SubtasksRepresentation - represents tree of TaskToTaskRelation models;

        * GroupRepresentation  - represents Group model;
        * GroupsRepresentation  - represents set of Group models;

        * PlanRepresentation - represents Plan model;
        * MessagesRepresentation  - represents set of Message models;
        * TaskDateRepresentation  - represents Date model;
        * AlertRepresentation  - represent Alert model;
"""

from typing import List

from pinecon.library.models.model.alert import Alert
from pinecon.library.models.model.group import Group
from pinecon.library.models.model.message import Message
from pinecon.library.models.model.plan import Plan
from pinecon.library.models.model.task import Task
from pinecon.library.models.model.task_date import TaskDate
from pinecon.library.models.model.user import User


class TaskRepresentation:
    """ Represents Task model. """

    def __init__(self, task: Task, creator: User, executors: List[User],
                 dates: List[TaskDate], groups: List[Group], alerts: List[Alert], plan: Plan):
        self.task = task
        self.creator = creator
        self.executors = executors
        self.dates = dates
        self.groups = groups
        self.alerts = alerts
        self.plan = plan


class TasksRepresentation:
    """ Represents set of Task models. """

    def __init__(self, creator: User, executor: User):
        self.creator = creator
        self.executor = executor


class SubtasksRepresentation:
    """ Represents tree of TaskToTaskRelation models. """

    def __init__(self, task: Task, relation_type: str, relation_id: str):
        self.task = task
        self.relation_type = relation_type
        self.relation_id = relation_id
        self.subtask_views_tree = []


class GroupRepresentation:
    """ Represents Group model. """

    def __init__(self, group: Group, group_tasks: List[Task]):
        self.group = group
        self.group_tasks = group_tasks


class GroupsRepresentation:
    """ Represents set of Group models. """

    def __init__(self, user_groups: List[Group]):
        self.user_groups = user_groups


class PlanRepresentation:
    """ Represents Plan model. """

    def __init__(self, plan: Plan, plan_task: Task):
        self.plan = plan
        self.plan_tasks = plan_task


class MessagesRepresentation:
    """ Represents set of Message models. """

    def __init__(self, messages: List[Message]):
        self.messages = messages


class TaskDateRepresentation:
    """ Represents Date model. """

    def __init__(self, date: TaskDate, task: Task):
        self.date = date
        self.task = task


class AlertRepresentation:
    """ Represent Alert model. """

    def __init__(self, alert: Alert, task: Task):
        self.alert = alert
        self.task = task
