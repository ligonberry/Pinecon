""" The module contains the basic functions for validation exceptions in the program.
All methods either return nothing (which means that the test was successful) or
raise exceptions (they can be found in the exception module)

    Classes:
    ----------------
        * DuplicationExceptionHandler
            - validate errors related to duplication of information;
        * LogicalExceptionHandler
            - validate logic-related errors
        * RightExceptionHandler
            - validate errors related to the lack of user rights to perform certain actions
"""
from datetime import datetime

from pinecon.library.models.model.executor_to_task import ExecutorToTaskRelation, ExecutorToTaskRelationAttribute
from pinecon.library.models.model.group_to_task import GroupToTaskRelation, GroupToTaskRelationAttribute
from pinecon.library.models.model.plan import Plan, PlanEndReason, PlanAttribute
from pinecon.library.models.model.task import Task, TaskStatus, TaskType
from pinecon.library.models.model.task_date import TaskDate, TaskDateType
from pinecon.library.models.model.task_to_task import TaskToTaskRelation, TaskRelation, TaskToTaskRelationAttribute

from pinecon.library.services import exceptions

from pinecon.library.services.loggers import log_function_call, log_exception

from pinecon.library.storage.queries import session_manager


@log_exception
@log_function_call
def is_object_exist(object_: object, object_id: int, object_name: str):
    """ Checks if such an object exists in the database. """
    if object_ is None:
        raise exceptions.NotFoundError(object_id, object_name)


class DuplicationExceptionHandler:
    """ This class catches errors related to duplication of information.

        Public Methods:
        ----------------
            * is_task_to_task_relation_duplication(self, task_to_task_relation: model.TaskToTaskRelation)
                - checks the task_to_task_relation for duplication;
            * is_group_to_task_relation_duplication(self, group_to_task_relation: model.GroupToTaskRelation)
                - checks the group_to_task_relation for duplication;
            * is_executor_to_task_duplication(self, executor_to_task_relation: model.ExecutorToTaskRelation)
                - checks the _executor_to_task_relation for duplication;
    """

    def __init__(self, data_storage: object):
        self.data_storage = data_storage

    @log_exception
    @log_function_call
    def is_task_to_task_relation_duplication(self, task_to_task_relation: TaskToTaskRelation):
        """ Checks the task_to_task_relation for duplication. """
        args = {
            TaskToTaskRelationAttribute.PARENT_TASK_ID.value: task_to_task_relation.parent_task_id,
            TaskToTaskRelationAttribute.SUBTASK_ID.value: task_to_task_relation.subtask_id
        }
        if self.data_storage.is_duplication(TaskToTaskRelation, args):
            raise exceptions.DuplicationError("Task-to-task relation is already exists!")

    @log_exception
    @log_function_call
    def is_group_to_task_relation_duplication(self, group_to_task_relation: GroupToTaskRelation):
        """ Checks the group_to_task_relation for duplication. """
        kwargs = {
            GroupToTaskRelationAttribute.GROUP_ID.value: group_to_task_relation.group_id,
            GroupToTaskRelationAttribute.TASK_ID.value: group_to_task_relation.task_id
        }

        if self.data_storage.is_duplication(GroupToTaskRelation, kwargs):
            raise exceptions.DuplicationError("Group-to-task relation is already exists!")

    @log_exception
    @log_function_call
    def is_executor_to_task_duplication(self, executor_to_task_relation: ExecutorToTaskRelation):
        """ Checks the _executor_to_task_relation for duplication. """
        args = {
            ExecutorToTaskRelationAttribute.EXECUTOR_ID.value: executor_to_task_relation.executor_id,
            ExecutorToTaskRelationAttribute.TASK_ID.value: executor_to_task_relation.task_id
        }

        if self.data_storage.is_duplication(ExecutorToTaskRelation, args):
            raise exceptions.DuplicationError("Executor-to-task relation is already exists!")


class LogicalExceptionHandler:
    """ The class catches logic-related errors: an attempt to add a circular dependency,
        an attempt to perform invalid operations, and so on.

        Public Methods:
        ----------------
            * is_task_can_be_executed(self, task: model.Task)
                - checks is task can be executed: is task does not have blocking
                  relation;
            * is_plan(self, task_id: int)
                - checks whether this operation is valid for the plan;
            * is_can_add_plan(cls, plan: model.Plan)
                - checks is it possible to add plan: is plan have end_reason and
                  logic value to it;
            * is_can_update_plan(args: list)
                - checks is it possible to update plan;
            * is_can_add_date(self, date: model.Date)
                - checks whether it is possible to add a date to the plan/task:
                  whether they have a date, whether this date is logical and so on.
            * is_can_update_date(self, date_id: int, new_date: model.Date)
                - checks whether it is possible to update a date to the plan/task:
                  whether they have a date, whether this date is logical and so on.
            * is_task_to_task_relation_has_loop(self, parent_task_id: int, subtask_id: int)
                - checks is task to task relation has loop: task1 -> task2, task2 -> task3,
                  so task3 could not -> task1;
    """

    def __init__(self, data_storage: object):
        self.data_storage = data_storage

    @log_exception
    @log_function_call
    def is_task_can_be_executed(self, task: Task):
        """ Checks is task can be executed: is task does not have blocking relation. """
        with session_manager() as session:

            relations = self.data_storage.get_all_task_to_task_relations_by_parent(task.task_id,
                                                                                   session=session)

            for relation in relations:

                # If subtask is not executed and relation is blocking -> raise exception
                if (relation.subtask.status != TaskStatus.EXECUTED.value and
                        relation.relation_type == TaskRelation.BLOCKING.value):
                    raise exceptions.LogicalError(
                        "You cannot execute a locked task until all of its subtasks have been executed!")

    @log_exception
    @log_function_call
    def is_plan(self, task_id: int):
        """ Checks whether this operation is valid for the plan. """
        task = self.data_storage.get_task_by_id(task_id)
        is_object_exist(task, task_id, "task")

        task = self.data_storage.get_task_by_id(task_id)

        if task.task_type == TaskType.PLAN.value:
            raise exceptions.LogicalError(f"This operation can't be applied to plan (id {task_id})")

    @classmethod
    @log_exception
    @log_function_call
    def is_can_add_plan(cls, plan: Plan):
        """ Checks is it possible to add plan: is plan have end_reason and logic value to it. """
        if (plan.end_reason == PlanEndReason.AFTER_DATE.value and
                plan.end_date is None):
            raise exceptions.LogicalError("Date, after which the plan will be stop, doesn't specified!")

        if (plan.end_reason == PlanEndReason.AFTER_NUMBER_OF_TIMES.value and
                plan.count is None):
            raise exceptions.LogicalError("Number of repetitions until the task is finished doesn't specified!")
        if LogicalExceptionHandler.is_start_end_dates_exist(plan):
            if (plan.start_date - plan.end_date).days > 0:
                raise exceptions.LogicalError("The end of the plan can not be less than the start date of the plan!")

    @classmethod
    @log_exception
    @log_function_call
    def is_start_end_dates_exist(cls, plan: Plan):
        if plan.start_date is not None and plan.end_date is not None:
            return True
        return False

    @staticmethod
    @log_exception
    @log_function_call
    def is_can_update_plan(args: dict):
        """ Checks is it possible to update plan: is plan have end_reason and logic value to it. """
        if PlanAttribute.END_REASON in args:
            end_reason = args[PlanAttribute.END_REASON]

            if (end_reason == PlanEndReason.AFTER_DATE.value and
                    PlanEndReason.AFTER_DATE.value not in args):
                raise exceptions.LogicalError("Number of repetitions until the task is finished doesn't specified!")

            if (end_reason == PlanEndReason.AFTER_NUMBER_OF_TIMES.value and
                    PlanAttribute.COUNT not in args):
                raise exceptions.LogicalError("Number of repetitions until the task is finished doesn't specified!")

    @log_exception
    @log_function_call
    def is_can_add_date(self, date: TaskDate):
        """
        Checks whether it is possible to add a date to the plan/task:
        whether they have a date, whether this date is logical and so on.

        """
        task = self.data_storage.get_task_by_id(date.task_id)

        is_object_exist(task, task.task_id, "task")

        task_dates = self.data_storage.get_all_task_dates(task.task_id)

        if date.date_type == TaskDateType.EVENT.value:
            for task_date in task_dates:
                if (task_date.date_type == TaskDateType.END.value or
                        task_date.date_type == TaskDateType.START.value):
                    raise exceptions.LogicalError("If task has period, you can't another event date")

                if task_date.date_type == date.date_type:
                    raise exceptions.LogicalError(
                        "You can't add one more date to the same task. Try to update exist task :) ")
        else:

            for task_date in task_dates:

                if task_date.date_type == TaskDateType.EVENT.value:
                    raise exceptions.LogicalError("Task already has event date, you can't make it pereodical!")

                if task_date.date_type == date.date_type:
                    raise exceptions.LogicalError(
                        "You can't add one more date to the same task. Try to update exist task :) ")

    @log_exception
    @log_function_call
    def is_can_update_date(self, date_id: int, new_date: datetime.date):
        """
        Checks whether it is possible to update a date to the plan/task:
        whether they have a date, whether this date is logical and so on.

        """
        date = self.data_storage.get_date_by_id(date_id)
        is_object_exist(date, date_id, "date")

        task_dates = self.data_storage.get_all_task_dates(date.task_id)

        if date.date_type == TaskDateType.START.value:
            for task_date in task_dates:

                if (task_date.date_type == TaskDateType.END.value and
                        new_date < datetime.now().date()):
                    raise exceptions.LogicalError(
                        "You can't set the end of the task in the past!")

        if date.date_type == TaskDateType.END.value:

            for task_date in task_dates:

                if (task_date.date_type == TaskDateType.START.value and
                        new_date < datetime.now().date()):
                    raise exceptions.LogicalError(
                        "You can't set the start of the task in the past!")

    @log_exception
    @log_function_call
    def is_task_to_task_relation_has_loop(self, parent_task_id: int, subtask_id: int):
        """
        Checks is task to task relation has loop: task1 -> task2, task2 -> task3,
        so task3 could not -> task1.

        """
        subtask = self.data_storage.get_task_by_id(subtask_id)
        is_object_exist(subtask, subtask_id, "subtask")

        parent_task = self.data_storage.get_task_by_id(parent_task_id)
        is_object_exist(parent_task, parent_task_id, "parent task")

        parents_id = set([task.task_id for task in self.data_storage.get_all_task_parent_tasks(subtask_id)])

        # Recursively add all parent id of parents and so on
        def add_parents(task_id):

            parent_tasks = self.data_storage.get_all_task_parent_tasks(task_id)

            for task in parent_tasks:
                add_parents(task.task_id)

            parents_id.add(task_id)

        add_parents(parent_task_id)

        # If subtask id is parent id, it forms a loop, so lets raise exception
        if subtask_id in parents_id:
            raise exceptions.LogicalError("Can't add task to task relation, as it forms a loop")


class RightExceptionHandler:
    """The class catches errors related to the lack of user rights
       to perform certain actions.

        Public Methods:
        ----------------
            * is_have_rights_task(self, user_id: int, task_id: int, executor=True)
                - checks whether the user has rights to work with this task;
            * is_have_rights_group(self, user_id: int, group_id: int)
                - checks whether the user has rights to work with this group;
            * is_have_rights_plan(self, user_id: int, plan_id: int)
                - checks whether the user has rights to work with this plan;
            * is_have_rights_message(self, user_id: int, message_id: int)
                - checks whether the user has rights to work with this message;
            * is_have_rights_date(self, user_id: int, date_id: int, executor=True)
                - checks whether the user has rights to work with this date;
            * is_have_rights_task_to_task_relation(self, user_id: int, parent_task_id: int,
                                             subtask_id: int, executor=True)
                - checks whether the user has rights to work with this parent task and
                  her subtask;
            * is_have_rights_task_to_task_relation_by_id(self, user_id: int, relation_id: int,
                                                   executor=True)
                - checks whether the user has rights to work with this task-to-task relation;
            * is_have_rights_group_to_task_relation(self, user_id: int, group_id: int, task_id: int)
                - checks whether the user has rights to work with this task and
                  her group;
            * is_have_rights_executor_to_task(self, user_id: int, relation_id: int)
                - checks whether the user has rights to work with this executor-to-task relation;
            * is_have_rights_alert(self, user_id: int, alert_id: int, executor=True)
                - checks whether the user has rights to work with task to which he would like to add alert;
    """

    def __init__(self, data_storage):
        self.data_storage = data_storage

    @log_exception
    @log_function_call
    def is_have_rights_task(self, user_id: int, task_id: int, executor=True):
        """ Checks whether the user has rights to work with this task. """
        task = self.data_storage.get_task_by_id(task_id)
        is_object_exist(task, task_id, "task")

        user = self.data_storage.get_user_by_id(user_id)
        is_object_exist(user, user_id, "user")

        if task.creator_id == user_id:
            return True

        if executor:
            executors = self.data_storage.get_executors(task_id)
            executors_id = set([user.user_id for user in executors])

            if user_id in executors_id:
                return True

        id_ = task_id
        raise exceptions.RightError(f"User doesn't have rights to work with task (id {id_})")

    @log_exception
    @log_function_call
    def is_have_rights_group(self, user_id: int, group_id: int):
        """ Checks whether the user has rights to work with this group. """
        group = self.data_storage.get_group_by_id(group_id)

        is_object_exist(group, group_id, "group")

        if group.creator_id != user_id:
            id_ = group.group_id
            raise exceptions.RightError(f"User doesn't have rights to work with group (id {id_})")

    @log_exception
    @log_function_call
    def is_have_rights_plan(self, user_id: int, plan_id: int):
        """ Checks whether the user has rights to work with this plan. """
        plan = self.data_storage.get_plan_by_id(plan_id)

        is_object_exist(plan, plan_id, "plan")

        task = self.data_storage.get_task_by_id(plan.task_id)

        if task.creator_id != user_id:
            raise exceptions.RightError(f"User doesn't have rights to work with plan (id {plan_id})")

    @log_exception
    @log_function_call
    def is_have_rights_message(self, user_id: int, message_id: int):
        """ Checks whether the user has rights to work with this message. """
        user = self.data_storage.get_user_by_id(user_id)
        is_object_exist(user, user_id, "user")

        message = self.data_storage.get_message_by_id(message_id)
        is_object_exist(message, message_id, "message")

        if message.user_id != user_id:
            raise exceptions.RightError(f"User doesn't have rights to work with this message (id {message_id})")

    @log_exception
    @log_function_call
    def is_have_rights_date(self, user_id: int, date_id: int, executor=True):
        """ Checks whether the user has rights to work with this date. """
        with session_manager() as session:
            date = self.data_storage.get_date_by_id(date_id, session=session)

            is_object_exist(date, date_id, "date")

            self.is_have_rights_task(user_id, date.task.task_id, executor)

    @log_exception
    @log_function_call
    def is_have_rights_task_to_task_relation(self, user_id: int, parent_task_id: int,
                                             subtask_id: int, executor=True):
        """
        Checks whether the user has rights to work with this
        parent task and her subtask.

        """
        self.is_have_rights_task(user_id, parent_task_id, executor)
        self.is_have_rights_task(user_id, subtask_id, executor)

    @log_exception
    @log_function_call
    def is_have_rights_task_to_task_relation_by_id(self, user_id: int, relation_id: int,
                                                   executor=True):
        """ Checks whether the user has rights to work with this task-to-task relation. """
        relation = self.data_storage.get_task_to_task_relation_by_id(relation_id)
        is_object_exist(relation, relation_id, "task to task relation")

        self.is_have_rights_task(user_id, relation.parent_task_id, executor)
        self.is_have_rights_task(user_id, relation.subtask_id, executor)

    @log_exception
    @log_function_call
    def is_have_rights_group_to_task_relation(self, user_id: int, group_id: int, task_id: int):
        """
        Checks whether the user has rights to work with this
        task and her group.

        """
        self.is_have_rights_task(user_id, task_id)
        self.is_have_rights_group(user_id, group_id)

    @log_exception
    @log_function_call
    def is_have_rights_executor_to_task(self, user_id: int, relation_id: int):
        """ Checks whether the user has rights to work with this executor-to-task relation. """
        relation = self.data_storage.get_executor_to_task_relation_by_id(relation_id)
        is_object_exist(relation, relation_id, "executor to task relation")

        task = self.data_storage.get_task_by_id(relation.task_id)
        if user_id != task.creator_id:
            raise exceptions.RightError(
                f"User doesn't have rights to add executor to task relations for task (id {user_id})")

    @log_exception
    @log_function_call
    def is_have_rights_alert(self, user_id: int, alert_id: int, executor=True):
        """ Checks whether the user has rights to work with task to which he would like to add alert. """
        with session_manager() as session:
            alert = self.data_storage.get_alert_by_id(alert_id, session=session)
            is_object_exist(alert, alert_id, "alert")

            task = alert.task
            self.is_have_rights_task(user_id, task.task_id, executor)
