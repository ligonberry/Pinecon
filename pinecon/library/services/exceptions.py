""" This module contains exceptions, that can rise within this package.

    PineconException - base exception for other class of exceptions;
    DuplicationError - raises, when user tries to add the same date (duplicate) to the database;
    DependencyError - raises, when user tries to do illegal relation operation;
    LogicalError - raises, when user tries to perform an illogical operation;
    NotFoundError - raises, when user trying to get an object that is not in the database;
    RightError - raises, when user trying to do something, he doesn't have a right to do;
"""


class PineconException(Exception):
    """ Base exception for other class of exceptions."""

    def __init__(self, message: str):
        """
        Parameters
        ----------
        message: str
            Exception text, which describes, what went wrong;
        """
        super().__init__(message)


class DuplicationError(PineconException):
    """
        Raises, when user tries to add the same date (duplicate) to the database.

        Examples:
        ----------
        * User trying to add duplicate group-to-task relation.
        * User trying to add duplicate task-to-task relation.
        * User trying to add duplicate executor-to-task relation.

    """
    def __init__(self, message: str):
        """
        Parameters
        ----------
        message: str
            Exception text, which describes, what went wrong;
        """
        super().__init__(message)


class LogicalError(PineconException):
    """
        Raises, when user tries to perform an illogical operation.

        Examples:
        ----------
            * Attempting to add end_date, which is earlier than start_date.
            * User trying to close task, in which subtasks does not closed.

    """
    def __init__(self, message: str):
        """
        Parameters
        ----------
        message: str
            Exception text, which describes, what went wrong;
        """
        super().__init__(message)


class NotFoundError(PineconException):
    """
         Raises, when user trying to get an object that is not in the database.

         Examples:
         ----------
            * User trying to get a task, before the creation.
            * User trying to get a remote plan.
    """
    def __init__(self, object_id: int, object_class: str):
        """
        Parameters
        ----------
        object_id: int
            Id of of object, that user trying to found.
        object_class: str
            Class of object (task, group, plan, ...), that user trying to found.
        """
        super().__init__(f"Couldn't find {object_class} with id {object_id}")


class RightError(PineconException):
    """
        Raises, when user trying to do something, he doesn't have a right to do.

        Examples:
        ----------
            * User trying to close another user's task.
            * User trying to delete another user's group.
            * User trying to add his task to another user's group.

    """

    def __init__(self, message: str):
        """
        Parameters
        ----------
        message: str
            Exception text, which describes, what went wrong;
        """
        super().__init__(message)
