""" The module contains a class and a set of functions for querying the database.

    Classes:
    ----------------
        Queries - contains core functions to make queries to database;
        session_manager(ContextDecorator) - context manager for working with sessions.

    Public Methods:
    ----------------
        session_decorator(function_to_decorate)
            - a decorator that inserts a session object into a function if it has not been provided;

"""
from contextlib import closing, ContextDecorator, ExitStack
from datetime import datetime
from functools import wraps

from sqlalchemy import and_
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import sessionmaker

from typing import List

from pinecon.library.services.loggers import log_function_call, log_exception

from pinecon.library.models.model.task import Task
from pinecon.library.models.model.group import Group
from pinecon.library.models.model.plan import Plan
from pinecon.library.models.model.message import Message
from pinecon.library.models.model.task_date import TaskDate
from pinecon.library.models.model.task_to_task import TaskToTaskRelation
from pinecon.library.models.model.group_to_task import GroupToTaskRelation
from pinecon.library.models.model.executor_to_task import ExecutorToTaskRelation
from pinecon.library.models.model.alert import Alert
from pinecon.library.models.model.user import User


# By default, the Session also expires all database loaded state on all ORM-managed
# attributes after transaction commit.
# This so that subsequent operations load the most recent data from the database.
# This behavior can be disabled using the expire_on_commit=False option to sessionmaker
# or the Session constructor.
# See more: http://qaru.site/questions/145637/sqlalchemy-detachedinstanceerror-with-regular-attribute-not-a-relation
# https://www.mail-archive.com/sqlalchemy@googlegroups.com/msg13278.html


Session = sessionmaker(expire_on_commit=False)


# For more information see
# https://alysivji.github.io/managing-resources-with-context-managers-pythonic.html
class session_manager(ContextDecorator):
    """ Context manager for working with sessions. """

    def __init__(self, session_cls=Session):
        self.session = session_cls()

    def __enter__(self):
        return self.session

    def __exit__(self, exc_type, value, traceback):
        with closing(self.session), ExitStack() as stack:
            stack.callback(self.session.rollback)
            if not value:
                self.session.commit()
                # If commit raises an exception, then rollback is left
                # in the exit stack.
                stack.pop_all()


def session_decorator(function_to_decorate):
    """ Decorator that inserts a session object into a function. """

    @log_exception
    @log_function_call
    @wraps(function_to_decorate)
    def wrapper_around_function(*args, **kwargs):
        if 'session' not in kwargs:
            try:
                with session_manager() as session:
                    return function_to_decorate(*args, session=session)
            except SQLAlchemyError as e:
                raise e
        else:
            if kwargs['session'] is None:
                raise AttributeError
            return function_to_decorate(*args, **kwargs)

    return wrapper_around_function


class Queries:
    """ Contains core functions to make queries to database.

        Attributes:
        ----------------
        * database (Storage)
            keeping data in database using SQLAlchemy ORM
        * engine (Engine)
            interpret the DBAPI’s module functions as well as the behavior of the database.

        Public Methods:
        ----------------
            1. Task queries:
                * add_task(self, task: Task, *, session=None) - add task object to the database;
                * get_task_by_id(self, task_id: int, session=None)
                                            - get task object from database by its id;
                * get_all_user_tasks(self, user_id: int, task_type: str = None, *, session=None)
                                            - get all task, that belong to defined user by id;
                * get_all_executor_tasks(self, user_id: int, task_type: str=None, *, session=None)
                                            - get all task, that belong to defined executor by id;
                * get_all_task_subtasks(self, task_id: int, *, session=None)
                                            - get all task subtasks, by task id;
                * get_all_task_parent_tasks(self, task_id: int, *, session=None)
                                            - get all task parent tasks, by task id;
                * delete_task_by_id(self, task_id: int, *, session=None)
                                            - delete task by its id;
                * update_task(self, task_id: int, args, *, session=None)
                                            - update information about task;
            2. Group queries:
                * add_group(self, group: Group, *, session=None) - add group object to the database;
                * get_group_by_id(self, group: id, session=None)
                                            - get group object from database by its id;
                * get_groups_by_user_id(self, user_id: int, *, session=None)
                                            - get group object from database by user id;
                * get_all_groups_by_task_id(self, user_id: int, task_id: int, *, session=None)
                                            - get all groups to which the task belongs, by task id;
                * get_all_group_tasks(self, group_id: int, *, session=None)
                                            - get all tasks that belong to the group, by group id;
                * delete_group_by_id(self, group_id: int, *, session=None)
                                            - delete group from database by its id;
                * delete_task_from_user_groups(self, user_id: int, task_id: int, *, session=None)
                                            - delete task by its id from all user groups;
                * update_group(self, group_id: int, args: 'an argument, which we want to update', *, session=None)
                                            - update information about group;
                * get_filtered_groups_by_task(self, filter_: dict, task_id: int = None)
                                            - return all groups that are associated with the task

            3. Plan queries:
                * add_plan(self, plan: Plan, *, session=None) - add plan object to the database;
                * get_plan_by_id(self, plan_id: int, session=None)
                                            - get plan object from database by its id;
                * get_task_plan(self, task_id: int, *, session=None)
                                            - get the task that is associated with the plan;
                * get_all_finished_plans(self, date=None, *, session=None)
                                            - get all plans that have ended by the current date;
                * get_all_user_plans(self, user_id: int, *, session=None)
                                            - get all user plans, by user id;
                * delete_plan_by_id(self, plan_id: int, *, session=None)
                                             - delete plan from database by its id;
                * update_plan(self, plan_id: int, args: 'an argument, which we want to update', *, session=None)
                                             - update information about plan;
            4. Message queries:
                * add_message(self, message: Message, *, session=None) - add message object to the database;
                * get_message_by_id(self, message_id: int, session=None)
                                             - get message object from database by its id;
                * delete_message_by_id(self, message_id: int, *, session=None)
                                             - delete message from database by its id;
            5. Date queries:
                * add_date(self, date: TaskDate, *, session=None) - add date object to the database;
                * get_date_by_id(self, date_id: int, *, session=None)
                                            - get date object from database by its id;
                * get_all_task_dates(self, task_id: int, *, session=None)
                                            - get all task dates by task id;
                * get_all_overdue_dates(self, date=None, *, session=None)
                                            - get all dates, that have passed to the present moment;
                * get_date_task(self, date_id: int, *, session=None)
                                            - get task by date id;
                * delete_date_by_id(self, date_id: int, *, session=None)
                                            - delete date from database by its id;
                * update_date(self, date_id: int, args: 'an argument, which we want to update', *, session=None)
                                            - update information about date;
            6. TaskToTaskRelation queries:
                * add_task_to_task_relation(self, task_to_task_relation: TaskToTaskRelation, *, session=None)
                                            - add task_to_task_relation object to the database;
                * get_task_to_task_relation_by_id(self, relation_id: int, *, session=None)
                                            - get task_to_task_relation object from database by its id;
                * get_task_to_task_relation(self, parent_task_id: int, subtask_id: int, *, session=None)
                                            - get task_to_task_reletion by parent and subtask ids;
                * get_all_task_to_task_relations_by_parent(self, parent_task_id: int, *, session=None)
                                            - get all parent task task_to_task_relations;
                * delete_task_to_task_relation_by_id(self, relation_id, *, session=None)
                                            - delete task_to_task_relation from database by its id;
                * delete_task_to_task_relation(self, parent_task_id: int, subtask_id: int,
                                               *, session=None)
                                            - delete task_to_task_relation from database by parent task
                                              and subtask ids;
                * update_task_to_task_relation(self, relation_id, args: 'an argument,
                                                which we want to update', *, session=None)
                                            - update information about task_to_task_relation;
            7. GroupToTaskRelations queries:
                * add_group_to_task_relation(self, group_to_task_relation: GroupToTaskRelation, *, session=None)
                                            - add group_to_task_relation object to the database;
                * get_group_to_task_relation_by_id(self, relation_id, *, session=None)
                                            - get group_to_task_relation object from database by its id;
                * delete_group_to_task_relation_by_id(self, relation_id: int, *, session=None)
                                            - delete group_to_task_relation from database by its id;
                * delete_group_to_task_relation(self, task_id: int, group_id: int, *, session=None)
                                            - delete group_to_task_relation from database by task and group ids;
                * update_group_to_task_relation(self, relation_id: int, args: 'an argument, which we want to update', *,
                                                session=None)
                                            - update information about group_to_task_relation;
            8. ExecutorToTaskRelation queries:
                * add_executor_to_task_relation(self, executor_to_task_relation: ExecutorToTaskRelation, *,
                                                session=None)
                                            - add executor_to_task_relation object to the database;
                * get_executor_to_task_relation_by_id(self, relation_id: int, *,
                                                     session=None)
                                            - get executor_to_task_relation object from database by its id;
                * delete_executor_to_task_relation_by_id(self, relation_id: int, *, session=None)
                                            - delete executor_to_task_relation from database by its id;
                * delete_executor_to_task_relation(self, user_id: int, task_id: int, *, session=None)
                                            - delete executor_to_task_relation from database by user
                                              and task ids;
                * update_executor_to_task_relation(self, relation_id: int,
                                         args: 'an argument, which we want to update', *, session=None)
                                         - update information about executor_to_task_relation;

            10. Alert queries:
                * add_alert(self, alert: Alert, *, session=None)
                                            - add alert object to the database;
                * get_alert_by_id(self, alert_id: int, *, session=None)
                                            - get alert object from database by its id;
                * get_alerts_by_task_id(self, task_id: int, *, session=None)
                                            - get alert object from database by task id;
                * get_alerts_by_user_id(self, user_id: int, *, session=None)
                                            - get alert object from database by user id;
                * get_alerts_by_executor_id(self, user_id: int, *, session=None)
                                            - get alert object from database by executor id;
                * get_all_overdue_alerts(self, date=None, time=None, *, session=None)
                                            - get all overdue to the current time alerts objects from database;
                * get_overdue_alerts_by_task_id(self, task_id, date=None, time=None, *, session=None)
                                            - get task overdue to the current time alerts objects from database;
                * get_executors_to_alert(self, alert_id, *, session=None)
                                            - get executors, which will be alerts;
                * delete_alert_by_id(self, alert_id, *, session=None)
                                            - delete alert from database by its id;
            11. User queries:
                * add_user(self, user: User, *, session=None)
                                            - add user object to the database;
                * get_user_by_id(self, user_id: int, *, session=None)
                                            - get user object from database by its id;
                * get_user_by_username(self, username: str, *, session=None)
                                            - get user object from database by its username;
                * get_users(self, *, session=None) - get all users object from database;
                * get_executors(self, task_id: int, *, session=None) - get all executors object from database;
                * delete_user_by_id(self, user_id: int, *, session=None)
                                            - delete user from database by its id;
                """

    def __init__(self, database):
        self.database = database

        self.engine = self.database.get_engine()
        Session.configure(bind=self.engine)

    # region Task's queries: add, get, delete and update tasks
    @session_decorator
    def add_task(self, task: Task, *, session=None) -> Task:
        session.add(task)
        return task

    @session_decorator
    def get_task_by_id(self, task_id: int, *, session=None) -> Task:
        return session.query(Task).get(task_id)

    @session_decorator
    def get_all_user_tasks(self, user_id: int, task_type: str = None, *, session=None) -> List[Task]:
        return (session.query(Task)
                .filter(and_(Task.creator_id == user_id,
                             (Task.task_type == task_type) if task_type is not None else True))).all()

    @session_decorator
    def get_all_executor_tasks(self, user_id: int, task_type: str = None, *, session=None) -> List[Task]:
        return (session.query(Task)
                .join(ExecutorToTaskRelation)
                .filter(and_(ExecutorToTaskRelation.executor_id == user_id,
                             (Task.task_type == task_type) if task_type is not None else True))).all()

    @session_decorator
    def get_all_task_subtasks(self, task_id: int, *, session=None) -> List[Task]:
        return (session.query(Task)
                .join(TaskToTaskRelation,
                      TaskToTaskRelation.subtask_id == Task.task_id)
                .filter(TaskToTaskRelation.parent_task_id == task_id)).all()

    @session_decorator
    def get_all_task_parent_tasks(self, task_id: int, *, session=None) -> List[Task]:
        return (session.query(Task)
                .join(TaskToTaskRelation,
                      TaskToTaskRelation.parent_task_id == Task.task_id)
                .filter(TaskToTaskRelation.subtask_id == task_id)).all()

    @session_decorator
    def delete_task_by_id(self, task_id: int, *, session=None):
        del_query = session.query(Task).filter_by(task_id=task_id)
        return self._delete_query(del_query)

    @session_decorator
    def update_task(self, task_id: int, args, *, session=None) -> Task:
        session.query(Task).filter_by(task_id=task_id).update(args)
        return session.query(Task).get(task_id)

    # endregion

    # region Group's queries: add, get, delete and update groups; delete task from group;
    @session_decorator
    def add_group(self, group: Group, *, session=None) -> Group:
        session.add(group)
        return group

    @session_decorator
    def get_group_by_id(self, group_id: int, *, session=None) -> Group:
        return session.query(Group).get(group_id)

    @session_decorator
    def get_groups_by_user_id(self, user_id: int, *, session=None) -> List[Group]:
        return session.query(Group).filter_by(creator_id=user_id).all()

    @session_decorator
    def get_all_groups_by_task_id(self, user_id: int, task_id: int, *, session=None) -> List[Group]:
        return (session.query(Group)
                .join(GroupToTaskRelation)
                .filter(and_(GroupToTaskRelation.task_id == task_id,
                             Group.creator_id == user_id))).all()

    @session_decorator
    def get_all_group_tasks(self, group_id: int, *, session=None) -> List[Task]:
        return (session.query(Task)
                .join(GroupToTaskRelation)
                .filter(GroupToTaskRelation.group_id == group_id)).all()

    @session_decorator
    def delete_group_by_id(self, group_id: int, *, session=None) -> bool:
        del_query = session.query(Group).filter_by(group_id=group_id)
        return self._delete_query(del_query)

    @session_decorator
    def delete_task_from_user_groups(self, user_id: int, task_id: int, *, session=None):
        relations_to_delete = (session.query(GroupToTaskRelation)
                               .join(Group).join(User)
                               .filter(and_(GroupToTaskRelation.task_id == task_id,
                                            User.user_id == user_id)))
        relations_ids_to_delete = [relation.relation_id for relation in relations_to_delete]
        for id_ in relations_ids_to_delete:
            session.query(GroupToTaskRelation).filter_by(relation_id=id_).delete()

    @session_decorator
    def update_group(self, group_id: int, args: 'group title', *, session=None) -> Group:
        session.query(Group).filter_by(group_id=group_id).update(args)
        return session.query(Group).get(group_id)

    @session_decorator
    def get_filtered_groups_by_task(self, filter_: dict, task_id: int = None, *, session=None) -> List[Group]:

        result_query = session.query(Group).filter_by(**filter_)

        if task_id is not None:
            result_query = (result_query.join(GroupToTaskRelation)
                            .filter(GroupToTaskRelation.task_id == task_id))

        return result_query

    # endregion

    # region Plan queries : add, get, delete and update plans;
    @session_decorator
    def add_plan(self, plan: Plan, *, session=None) -> Plan:
        session.add(plan)
        return plan

    @session_decorator
    def get_plan_by_id(self, plan_id: int, *, session=None) -> Plan:
        return session.query(Plan).get(plan_id)

    @session_decorator
    def get_task_plan(self, task_id: int, *, session=None) -> Plan:
        return session.query(Plan).filter_by(task_id=task_id).first()

    @session_decorator
    def get_all_finished_plans(self, date=None, *, session=None) -> List[Plan]:
        date = datetime.now().date() if not date else date
        return session.query(Plan).filter(and_(date >= Plan.start_date,
                                               Plan.active == 'yes')).all()

    @session_decorator
    def get_all_user_plans(self, user_id: int, *, session=None) -> List[Plan]:
        return (session.query(Plan)
                .join(Task)
                .filter(Task.creator_id == user_id)).all()

    @session_decorator
    def delete_plan_by_id(self, plan_id: int, *, session=None):
        del_query = session.query(Plan).filter_by(plan_id=plan_id)
        return self._delete_query(del_query)

    @session_decorator
    def update_plan(self, plan_id: int, args: 'an argument, which we want to update', *, session=None) -> Plan:
        session.query(Plan).filter_by(plan_id=plan_id).update(args)
        return session.query(Plan).get(plan_id)

    # endregion

    # region Messages queries: add, get and delete messages;
    @session_decorator
    def add_message(self, message: Message, *, session=None) -> Message:
        session.add(message)
        return message

    @session_decorator
    def get_message_by_id(self, message_id: int, *, session=None) -> Message:
        return session.query(Message).get(message_id)

    @session_decorator
    def get_messages_by_user_id(self, user_id: int, *, session=None) -> Message:
        return (session.query(Message)
                .filter_by(user_id=user_id).all())

    @session_decorator
    def delete_message_by_id(self, message_id: int, *, session=None):
        del_query = session.query(Message).filter_by(message_id=message_id)
        return self._delete_query(del_query)

    # endregion;

    # region Date queries: add, get, delete and update dates;
    @session_decorator
    def add_date(self, date: TaskDate, *, session=None) -> TaskDate:
        session.add(date)
        return date

    @session_decorator
    def get_date_by_id(self, date_id: int, *, session=None) -> TaskDate:
        return session.query(TaskDate).get(date_id)

    @session_decorator
    def get_all_task_dates(self, task_id: int, *, session=None) -> List[TaskDate]:
        return session.query(TaskDate).filter_by(task_id=task_id).all()

    @session_decorator
    def get_all_overdue_dates(self, date=None, *, session=None) -> List[TaskDate]:
        date = datetime.now().date() if not date else date
        return session.query(TaskDate).filter(TaskDate.date < date).all()

    @session_decorator
    def get_date_task(self, date_id: int, *, session=None) -> Task:
        return (session.query(Task)
                .join(TaskDate)
                .filter(TaskDate.date_id == date_id)).first()

    @session_decorator
    def delete_date_by_id(self, date_id: int, session=None):
        del_query = session.query(TaskDate).filter_by(date_id=date_id)
        return self._delete_query(del_query)

    @session_decorator
    def update_date(self, date_id: int, args: 'an argument, which we want to update', *,
                    session=None) -> TaskDate:
        session.query(TaskDate).filter_by(date_id=date_id).update(args)
        return session.query(TaskDate).get(date_id)

    # endregion

    # region TaskToTaskRelation queries: add, get, delete and update task-to-task relations;
    @session_decorator
    def add_task_to_task_relation(self, task_to_task_relation: TaskToTaskRelation, *,
                                  session=None) -> TaskToTaskRelation:
        session.add(task_to_task_relation)
        return task_to_task_relation

    @session_decorator
    def get_task_to_task_relation_by_id(self, relation_id: int, *,
                                        session=None) -> TaskToTaskRelation:
        return session.query(TaskToTaskRelation).get(relation_id)

    @session_decorator
    def get_task_to_task_relation(self, parent_task_id: int, subtask_id: int, *,
                                  session=None) -> TaskToTaskRelation:
        return (session.query(TaskToTaskRelation)
                .filter_by(parent_task_id=parent_task_id, subtask_id=subtask_id)
                .first())

    @session_decorator
    def get_all_task_to_task_relations_by_parent(self, parent_task_id: int, *,
                                                 session=None) -> List[TaskToTaskRelation]:
        return session.query(TaskToTaskRelation).filter_by(parent_task_id=parent_task_id).all()

    @session_decorator
    def delete_task_to_task_relation_by_id(self, relation_id: int, *, session=None):
        del_query = session.query(TaskToTaskRelation).filter_by(relation_id=relation_id)
        return self._delete_query(del_query)

    @session_decorator
    def delete_task_to_task_relation(self, parent_task_id: int, subtask_id: int, *, session=None):
        del_query = (session.query(TaskToTaskRelation)
                     .filter(and_(TaskToTaskRelation.subtask_id == subtask_id,
                                  TaskToTaskRelation.parent_task_id == parent_task_id)))
        return self._delete_query(del_query)

    @session_decorator
    def update_task_to_task_relation(self, relation_id, args: 'an argument, which we want to update', *,
                                     session=None) -> TaskToTaskRelation:
        session.query(TaskToTaskRelation).filter_by(relation_id=relation_id).update(args)
        return session.query(TaskToTaskRelation).get(relation_id)

    # endregion

    # region GroupToTaskRelation queries: add, get, delete and update group-to-task relations;
    @session_decorator
    def add_group_to_task_relation(self, group_to_task_relation: GroupToTaskRelation, *,
                                   session=None) -> GroupToTaskRelation:
        session.add(group_to_task_relation)
        return group_to_task_relation

    @session_decorator
    def get_group_to_task_relation_by_id(self, relation_id: int, *,
                                         session=None) -> GroupToTaskRelation:
        return session.query(GroupToTaskRelation).get(relation_id)

    @session_decorator
    def delete_group_to_task_relation_by_id(self, relation_id, *,
                                            session=None):
        delete_query = session.query(GroupToTaskRelation).filter_by(relation_id=relation_id)
        return self._delete_query(delete_query)

    @session_decorator
    def delete_group_to_task_relation(self, task_id, group_id, *, session=None):
        delete_query = (session.query(GroupToTaskRelation)
                        .filter(and_(GroupToTaskRelation.task_id == task_id,
                                     GroupToTaskRelation.group_id == group_id)))
        return self._delete_query(delete_query)

    @session_decorator
    def update_group_to_task_relation(self, relation_id, args: 'an argument, which we want to update', *,
                                      session=None) -> GroupToTaskRelation:
        session.query(GroupToTaskRelation).filter_by(relation_id=relation_id).update(args)
        return session.query(GroupToTaskRelation).get(relation_id)

    # endregion

    # region ExecutorToTaskRelation queries: add, get, delete and update executor-to-task relations;
    @session_decorator
    def add_executor_to_task_relation(self, executor_to_task_relation: ExecutorToTaskRelation, *,
                                      session=None) -> ExecutorToTaskRelation:
        session.add(executor_to_task_relation)
        return executor_to_task_relation

    @session_decorator
    def get_executor_to_task_relation_by_id(self, relation_id: int, *,
                                            session=None) -> ExecutorToTaskRelation:
        return session.query(ExecutorToTaskRelation).get(relation_id)

    @session_decorator
    def delete_executor_to_task_relation_by_id(self, relation_id: int, *, session=None):
        del_query = session.query(ExecutorToTaskRelation).filter_by(relation_id=relation_id)
        return self._delete_query(del_query)

    @session_decorator
    def delete_executor_to_task_relation(self, user_id: int, task_id: int, *, session=None):
        del_query = (session.query(ExecutorToTaskRelation)
                     .filter(and_(ExecutorToTaskRelation.task_id == task_id,
                                  ExecutorToTaskRelation.executor_id == user_id)))
        return self._delete_query(del_query)

    @session_decorator
    def update_executor_to_task_relation(self, relation_id: int,
                                         args: 'an argument, which we want to update', *,
                                         session=None) -> ExecutorToTaskRelation:
        session.query(ExecutorToTaskRelation).filter_by(relation_id=relation_id).update(args)
        return session.query(ExecutorToTaskRelation).get(relation_id)

    # endregion

    # region Alert queries: add and get alerts
    @session_decorator
    def add_alert(self, alert: Alert, *, session=None) -> Alert:
        session.add(alert)
        return alert

    @session_decorator
    def get_alert_by_id(self, alert_id: int, *, session=None) -> Alert:
        return session.query(Alert).get(alert_id)

    @session_decorator
    def get_alerts_by_task_id(self, task_id: int, *, session=None) -> List[Alert]:
        return session.query(Alert).filter_by(task_id=task_id).all()

    @session_decorator
    def get_alerts_by_user_id(self, user_id: int, *, session=None) -> List[Alert]:
        return (session.query(Alert)
                .join(Task)
                .filter(Task.creator_id == user_id)).all()

    @session_decorator
    def get_alerts_by_executor_id(self, user_id: int, *, session=None) -> List[Alert]:
        return (session.query(Alert)
                .join(Task)
                .join(ExecutorToTaskRelation)
                .filter(ExecutorToTaskRelation.executor_id == user_id)).all()

    @session_decorator
    def get_all_overdue_alerts(self, date=None, *, session=None) -> List[Alert]:
        date = datetime.now().date() if not date else date
        return self._helper_get_all_overdue_alerts(session, date).all()

    @staticmethod
    def _helper_get_all_overdue_alerts(session, date):
        return (session.query(Alert)
                .filter(and_(date >= Alert.start_date,
                             Alert.active == "yes")))

    @session_decorator
    def get_overdue_alerts_by_task_id(self, task_id: int, date=None, *, session=None) -> List[Alert]:
        date = datetime.now().date() if not date else date
        overdue_alerts = self._helper_get_all_overdue_alerts(session, date)
        return overdue_alerts.filter_by(task_id=task_id).all()

    @session_decorator
    def get_executors_to_alert(self, alert_id: int, *, session=None) -> List[User]:
        return (session.query(User)
                .join(ExecutorToTaskRelation)
                .join(Task)
                .join(Alert)
                .filter(Alert.alert_id == alert_id)).all()

    @session_decorator
    def delete_alert_by_id(self, alert_id: int, *, session=None):
        del_query = session.query(Alert).filter_by(alert_id=alert_id)
        return self._delete_query(del_query)

    @session_decorator
    def update_alert(self, alert_id: int, args: list, *, session=None) -> Alert:
        session.query(Alert).filter_by(alert_id=alert_id).update(args)
        return session.query(Alert).get(alert_id)

    # endregion

    # region User queries: add, get and delete users
    @session_decorator
    def add_user(self, user: User, *, session=None) -> User:
        session.add(user)
        return user

    @session_decorator
    def get_user_by_id(self, user_id: int, *, session=None) -> User:
        return session.query(User).get(user_id)

    @session_decorator
    def get_user_by_username(self, username: str, *, session=None) -> User:
        return session.query(User).filter_by(name=username).first()

    @session_decorator
    def get_users(self, *, session=None) -> List[User]:
        return session.query(User).all()

    # Executors - also users, but user can create and give to perform the task.
    # So, executors is someone, who is working on a task (his own or the one he has received)
    @session_decorator
    def get_executors(self, task_id: int, *, session=None) -> List[User]:
        return (session.query(User)
                .join(ExecutorToTaskRelation)
                .filter(ExecutorToTaskRelation.task_id == task_id)).all()

    @session_decorator
    def delete_user_by_id(self, user_id: int, *, session=None):
        del_query = session.query(User).filter_by(user_id=user_id)
        return self._delete_query(del_query)

    # endregion

    @staticmethod
    def _delete_query(query: list) -> bool:
        result = query.all()
        if result:
            query.delete()
            return True
        else:
            return False

    @session_decorator
    def is_duplication(self, model, kwargs, session=None):
        return session.query(model).filter_by(**kwargs).first() is not None
