"""
This module provides class for keeping data in database using SQLAlchemy ORM

    Classes:
    ----------------
        Storage - contains core functions to keeping data

A Classical Mapping refers to the configuration of a mapped class using the mapper() function,
without using the Declarative system. This is SQLAlchemy’s original class mapping API,
and is still the base mapping system provided by the ORM.

For see reasons, why I choice explicit syntax for mapper objects:
https://stackoverflow.com/questions/1453219/should-i-create-mapper-objects-or-use-the-declarative-syntax-in-sqlalchemy
"""
from sqlalchemy import Date, Column, Integer, Table, Time, String, ForeignKey, text
from sqlalchemy import MetaData
from sqlalchemy import create_engine
from sqlalchemy.orm import mapper, clear_mappers
from sqlalchemy.orm import relationship

from pinecon.library.models.model.task import Task
from pinecon.library.models.model.group import Group
from pinecon.library.models.model.plan import Plan
from pinecon.library.models.model.message import Message
from pinecon.library.models.model.task_date import TaskDate
from pinecon.library.models.model.task_to_task import TaskToTaskRelation
from pinecon.library.models.model.group_to_task import GroupToTaskRelation
from pinecon.library.models.model.executor_to_task import ExecutorToTaskRelation
from pinecon.library.models.model.alert import Alert
from pinecon.library.models.model.user import User


class Storage:
    """ Contains core functions to keeping data."""

    def __init__(self, connection_string):
        """
        Parameters
        ----------
        connection_string: str
            Connection_string of current user database;
        """
        self.engine = self._create_database(connection_string)

        # Container object that keeps together many different features of a database (or multiple databases).
        self.metadata = MetaData(self.engine)

        self._create_tables()

        # Actually like to create our selection of tables for real inside the SQLite database.
        self.metadata.create_all(self.engine)

        self._create_mappers()

    def get_engine(self):
        """ Send engine object of the database to Queries class.

        Returns
        ----------
        engine : object
            interpret the DBAPI’s module functions as well as the behavior of the database.
        """
        return self.engine

    @staticmethod
    def _create_database(connection_string):
        return create_engine(connection_string)

    def _create_tables(self):
        """ Create tables.

        Attributes:
        ------------
        table_name: str
        metadata: MetaData object
            will be associated with table name

        Multiple columns may be assigned the primary_key=True flag,
        which denotes a multi-column primary key, known as a composite primary key.

        """
        self.task_table = Table('Tasks', self.metadata,
                                Column('creator_id', ForeignKey('Users.user_id', ondelete='CASCADE'), nullable=False,
                                       index=True),
                                Column('title', String, nullable=False),

                                Column('task_id', Integer, primary_key=True, nullable=False),
                                Column('status', String, nullable=False, server_default=text("'created'")),
                                Column('task_type', String, nullable=False),
                                Column('priority', String, nullable=False, server_default=text("'mid'")),
                                Column('description', String),
                                Column('subtask_dependency', String, nullable=False, server_default=text("'no'")),
                                Column('creation_date', Date)
                                )

        self.group_table = Table('Groups', self.metadata,
                                 Column('creator_id', ForeignKey('Users.user_id', ondelete='CASCADE'),
                                        index=True, nullable=False),
                                 Column('title', String, nullable=False),

                                 Column('group_id', Integer, primary_key=True),
                                 Column('group_type', String, nullable=False, server_default=text("'custom'"))
                                 )

        self.plan_table = Table('Plans', self.metadata,
                                Column('start_date', Date, nullable=False),
                                Column('repeat_style', String, nullable=False),
                                Column('interval_counter', Integer),
                                Column('end_reason', String, nullable=False),

                                Column('plan_id', Integer, primary_key=True),
                                Column('end_date', Date),
                                Column('count', Integer),
                                Column('task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'),
                                       index=True),
                                Column('counter', Integer, nullable=False, server_default=text("'0'")),
                                Column('active', String, nullable=False, server_default=text("'yes'"))
                                )

        self.message_table = Table('Messages', self.metadata,
                                   Column('user_id', ForeignKey('Users.user_id', ondelete='CASCADE'), nullable=False,
                                          index=True),
                                   Column('title', String, nullable=False),

                                   Column('message_id', Integer, primary_key=True),
                                   Column('text', String, nullable=False),
                                   Column('date', Date, nullable=False),
                                   Column('time', Time, nullable=False)
                                   )

        self.date_table = Table('Dates', self.metadata,
                                Column('task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False,
                                       index=True),
                                Column('date_type', Integer, nullable=False),
                                Column('date', Date, nullable=False),

                                Column('date_id', Integer, primary_key=True)
                                )

        self.task_to_task_relation_table = Table('TaskToTaskRelations', self.metadata,
                                                 Column('parent_task_id',
                                                        ForeignKey('Tasks.task_id', ondelete='CASCADE'),
                                                        nullable=False, index=True),
                                                 Column('subtask_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'),
                                                        nullable=False, index=True),

                                                 Column('relation_id', Integer, primary_key=True),
                                                 Column('relation_type', Integer, nullable=False)
                                                 )

        self.group_to_task_relation_table = Table('GroupToTaskRelations', self.metadata,
                                                  Column('group_id', ForeignKey('Groups.group_id', ondelete='CASCADE'),
                                                         nullable=False, index=True),
                                                  Column('task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'),
                                                         nullable=False, index=True),

                                                  Column('relation_id', Integer, primary_key=True)
                                                  )

        self.executor_to_task_relation_table = Table('ExecutorToTaskRelations', self.metadata,
                                                     Column('executor_id',
                                                            ForeignKey('Users.user_id', ondelete='CASCADE'),
                                                            index=True,
                                                            nullable=False),
                                                     Column('task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'),
                                                            nullable=False, index=True),

                                                     Column('relation_id', Integer, primary_key=True),
                                                     Column('relation_confirmed', String, nullable=False,
                                                            server_default=text("'no'"))
                                                     )

        self.alert_table = Table('Alert', self.metadata,
                                 Column('task_id', ForeignKey('Tasks.task_id', ondelete='CASCADE'), nullable=False,
                                        index=True),
                                 Column('start_date', Date, nullable=False),
                                 Column('date_interval', Integer, nullable=False),

                                 Column('alert_id', Integer, primary_key=True),
                                 Column('active', String, nullable=False,
                                        server_default=text("'yes'"))
                                 )

        self.user_table = Table('Users', self.metadata,
                                Column('user_id', Integer, primary_key=True),
                                Column('name', String)
                                )

    def _create_mappers(self):
        """ Assemble all the columns in the mapped Table into mapped object attributes,
        each of which are named according to the name of the column itself
        (specifically, the key attribute of Column).

        Attributes:
        ------------
        class: class object
        table: table object
        properties: dict
            bidirectional relationship with a scalar attribute on both sides
        """
        clear_mappers()

        mapper(Task, self.task_table, properties={
            "creator": relationship(User)
        })

        mapper(Group, self.group_table, properties={
            "creator": relationship(User)
        })

        mapper(Plan, self.plan_table, properties={
            "task": relationship(Task)
        })

        mapper(Message, self.message_table, properties={
            "user": relationship(User, primaryjoin=self.message_table.c.user_id == self.user_table.c.user_id)
        })

        mapper(TaskDate, self.date_table, properties={
            "task": relationship(Task)
        })

        mapper(TaskToTaskRelation, self.task_to_task_relation_table, properties={
            "parent_task": relationship(Task,
                                        primaryjoin=self.task_to_task_relation_table.c.parent_task_id == self.task_table.c.task_id),
            "subtask": relationship(Task,
                                    primaryjoin=self.task_to_task_relation_table.c.subtask_id == self.task_table.c.task_id)
        })

        mapper(GroupToTaskRelation, self.group_to_task_relation_table, properties={
            "group": relationship(Group),
            "task": relationship(Task)
        })

        mapper(ExecutorToTaskRelation, self.executor_to_task_relation_table, properties={
            "executor": relationship(User),
            "task": relationship(Task)
        })

        mapper(Alert, self.alert_table, properties={
            "task": relationship(Task)
        })

        mapper(User, self.user_table)
