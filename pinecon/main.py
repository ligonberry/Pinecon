import json
import os
from pathlib import Path

from pinecon import settings
from pinecon.library.models.model.user import User
from pinecon.library.services import loggers
from pinecon.library.services.service import Service
from pinecon.library.storage.queries import Queries
from pinecon.library.storage.storage import Storage
from pinecon.terminal.handling.handler import TerminalMenu
from pinecon.terminal.initialization import parser
from pinecon.terminal.initialization.commands import Commands


def main():
    os.makedirs(settings.PINECON_PATH, exist_ok=True)

    setup_logger()

    namespace = parser.get_parsed_data()

    if getattr(namespace, Commands.FIRST) == Commands.INITIALISATION:
        TerminalMenu.handle_init_command()
        return

    if getattr(namespace, Commands.FIRST) == Commands.LOGIN:
        TerminalMenu.handle_login_command(namespace)
        return

    user = read_user_data()
    if user is None:
        return
    user_id = user.user_id

    data_storage = Storage(settings.CONNECTION_STRING)
    query = Queries(data_storage)
    service = Service(query, current_user_id=user_id)

    service.update_application()

    TerminalMenu.handle(service, namespace)


def read_user_data(filename=settings.USER_DATA_FILE):
    try:
        with open(filename) as json_file:
            user_data = json.load(json_file)
    except FileNotFoundError:
        print("No active user. Try 'pine login -u username'")
        return

    current_user_id = user_data['current_user_id']
    current_user_name = user_data['current_user']
    user = User(current_user_name, user_id=current_user_id)
    return user


def _write_default_log_config():
    default_config = {
        'level': loggers.LogLevel.DEBUG.value,
        'format': '[%(levelname)s] %(asctime)s - %(name)s: %(message)s',
        'path': os.path.join(settings.PINECON_PATH, 'pinelog.log')
    }
    with open(settings.LOG_CONFIG, 'w') as outfile:
        json.dump(default_config, outfile, indent=4)


def _read_log_config():
    if not Path(settings.LOG_CONFIG).exists():
        _write_default_log_config()
    with open(settings.LOG_CONFIG) as json_file:
        log_config = json.load(json_file)
    return log_config


def setup_logger():
    log_config = _read_log_config()

    loggers.set_logger(log_level=log_config['level'],
                       log_format=log_config['format'],
                       log_path=log_config['path'])
