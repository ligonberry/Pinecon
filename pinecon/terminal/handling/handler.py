# coding=utf-8
""" This module contains a class that is responsible for transmitting data received from the user to functions
    that are in other modules for further processing.
"""
import json

from sqlalchemy.exc import SQLAlchemyError

from pinecon import settings
from pinecon.library.models.model.alert import Alert
from pinecon.library.models.model.executor_to_task import ExecutorToTaskRelation
from pinecon.library.models.model.group import Group
from pinecon.library.models.model.group_to_task import GroupToTaskRelation
from pinecon.library.models.model.plan import Plan, PlanEndReason
from pinecon.library.models.model.task import Task, TaskStatus, TaskType
from pinecon.library.models.model.task_date import TaskDate, TaskDateType
from pinecon.library.models.model.task_to_task import TaskToTaskRelation
from pinecon.library.models.model.user import User

from pinecon.library.services.exceptions import PineconException
from pinecon.library.services.service import Service
from pinecon.library.storage.queries import Queries
from pinecon.library.storage.storage import Storage

from pinecon.terminal.handling.printers import Printers

from pinecon.terminal.initialization.commands import Commands


def error_manager(func):
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except SQLAlchemyError:
            print("Storage error")
        except PineconException as pe:
            print(pe)

    return wrapper


class TerminalMenu:
    """Transmitting data received from the user to functions
       that are in other modules for further processing.
    """
    service = None

    @staticmethod
    def handle_init_command(connection_string=settings.CONNECTION_STRING):
        print("The database was successfully created")
        Storage(connection_string)

    @staticmethod
    def handle_login_command(namespace, connection_string=settings.CONNECTION_STRING):
        storage = Storage(connection_string)
        query = Queries(storage)
        service = Service(query)

        username = namespace.username

        if not service.get_user_by_username(username):
            print(f"The user {username} was successfully added to the database")
            service.add_checked_user(User(username))
        print(f"Hi, {username}")

        user = service.get_user_by_username(username)
        TerminalMenu.write_user_data(user)

    @staticmethod
    def write_user_data(user, filename=settings.USER_DATA_FILE):
        user_data = {'current_user_id': user.user_id,
                     'current_user': user.name}
        with open(filename, 'w') as outfile:
            json.dump(user_data, outfile)

    @staticmethod
    @error_manager
    def handle_command(commands, command, *args, **kwargs):
        commands[command](*args, **kwargs)

    @staticmethod
    def handle(service, namespace):
        if namespace.first_command is None:
            print("Illegal command. Use `pine -h to` see the help message.")
            return
        TerminalMenu.service = service
        TerminalMenu.handle_command(TerminalMenu.handlers, namespace.first_command, namespace=namespace)

    # region Task: create, show, delete and update task;
    @staticmethod
    def handle_task(namespace, *args, **kwargs):
        TerminalMenu.handle_command(TerminalMenu.task_handlers, namespace.second_command, namespace=namespace, *args,
                                    **kwargs)

    @staticmethod
    def handle_create_task(namespace):
        creator_id = TerminalMenu.service.current_user_id

        task = Task(creator_id=creator_id, title=namespace.title,
                    description=namespace.description, priority=namespace.priority,
                    subtask_dependency=namespace.sub_dependent)

        task = TerminalMenu.service.add_checked_task(task)
        print(f"Task was successfully created with id {task.task_id}"
              f"\nYou can see task, enter pine task show -i {task.task_id}\n")

        incompatible_date_selection = namespace.date and (namespace.start_date or namespace.end_date)
        if incompatible_date_selection:
            print("Wrong dates. You try to add `date` and `start date`."
                  "\nSelect one date type."
                  "\nTask was successfully created, but date was't."
                  "\nTry to update task, if you want to add date to her."
                  f"\nEnter `pine task update -i {task.task_id}` to update created task.")
        elif namespace.end_date or namespace.start_date or namespace.date:
            one_date_creation = ((namespace.end_date and not namespace.start_date) or
                                 (namespace.start_date and not namespace.end_date) or
                                 namespace.date)
            if one_date_creation:
                if namespace.date:
                    date = TaskDate(task_id=task.task_id, date_type=TaskDateType.EVENT.value, date=namespace.date)
                else:
                    if namespace.end_date and not namespace.start_date:
                        date = TaskDate(task_id=task.task_id, date_type=TaskDateType.END.value,
                                        date=namespace.end_date)
                    else:
                        date = TaskDate(task_id=task.task_id, date_type=TaskDateType.START.value,
                                        date=namespace.start_date)
                date = TerminalMenu.service.add_checked_date(date)
                print(f"Date was successfully created with id {date.date_id}"
                      f"\nIf you want to see this date enter `pine date show -i {date.date_id}`")
            else:
                if namespace.end_date < namespace.start_date:
                    print("Start date can't be later than end date, dates weren't added")
                else:
                    start_date = TaskDate(task_id=task.task_id, date_type=TaskDateType.START.value,
                                          date=namespace.start_date)
                    end_date = TaskDate(task_id=task.task_id, date_type=TaskDateType.END.value,
                                        date=namespace.end_date)
                    start_date = TerminalMenu.service.add_checked_date(start_date)
                    end_date = TerminalMenu.service.add_checked_date(end_date)
                    print(f"Start date was successfully created with id {start_date.date_id}"
                          f"\nIf you want to see this date enter `pine date show -i {start_date.date_id}`")
                    print(f"\nEnd date was successfully created with id {end_date.date_id}"
                          f"\nIf you want to see this date enter `pine date show -i {end_date.date_id}`")

    @staticmethod
    def handle_show_task(namespace):
        task_id = namespace.task_id
        task_view = TerminalMenu.service.view_task(task_id)
        print("      Task")
        Printers.show_task(task_view.task)
        if task_view.plan:
            print("      Plan")
            Printers.show_plan(task_view.plan)
        print("      Creator")
        Printers.show_user(task_view.creator)

        if task_view.dates:
            print("       Dates")
            for date in task_view.dates:
                Printers.show_date(date)
        if task_view.groups:
            print("       Groups")
            for group in task_view.groups:
                Printers.show_group(group)
        if task_view.executors:
            print("       Executors")
            for executor in task_view.executors:
                Printers.show_user(executor)
        if task_view.alerts:
            print("       Alerts")
            for alert in task_view.alerts:
                Printers.show_alert(alert)

    @staticmethod
    def handle_update_task(namespace):
        task_id = namespace.task_id
        task = TerminalMenu.service.update_checked_task(task_id,
                                                        title=namespace.title,
                                                        description=namespace.description,
                                                        priority=namespace.priority,
                                                        subtask_dependency=namespace.sub_dependent,
                                                        status=namespace.status)
        print(f"Task was successfully updated with id {task.task_id}"
              f"\nYou can see task, enter `pine task show -i {task.task_id}`\n")

        incompatible_date_selection = (namespace.date and (namespace.end_date or namespace.start_date))
        if incompatible_date_selection:
            print("Wrong dates. You try to add date and start date. "
                  "\nSelect one date type."
                  "\nTask was successfully created, but date was't."
                  "\nTry to update task, if you want to add date to her."
                  f"\nEnter `pine task update -i {task_id}` to update created task.")
        elif namespace.date or namespace.start_date or namespace.end_date:
            task_dates = TerminalMenu.service.get_all_checked_task_dates(task_id)
            if task_dates:
                if len(task_dates) == 1:
                    task_date = task_dates[0]
                    if task_date.date_type == TaskDateType.EVENT.value and namespace.date is not None:
                        task_date = TerminalMenu.service.update_checked_date(task_date.date_id, date=namespace.date)
                    elif task_date.date_type == TaskDateType.START.value and namespace.start_date is not None:
                        task_date = TerminalMenu.service.update_checked_date(task_date.date_id,
                                                                             date=namespace.start_date)
                    elif task_date.date_type == TaskDateType.END.value and namespace.end_date is not None:
                        task_date = TerminalMenu.service.update_checked_date(task_date.date_id, date=namespace.end_date)
                    else:
                        print("Date not owned by task")
                        return
                    print("Date was successfully updated"
                          f"\nIf you want to see this date enter `pine date show -i {task_date.date_id}`")
                else:
                    start_date = task_dates[0] if task_dates[0].date_type == TaskDateType.START.value else task_dates[1]
                    end_date = task_dates[1] if task_dates[1].date_type == TaskDateType.END.value else task_dates[0]

                    if namespace.start_date:
                        start_date = TerminalMenu.service.update_checked_date(start_date.date_id,
                                                                              date=namespace.start_date)
                        print("Start date was successfully updated"
                              f"\nIf you want to see this date enter "
                              f"`pine date show -i {start_date.date_id}`")
                    if namespace.end_date:
                        end_date = TerminalMenu.service.update_checked_date(end_date.date_id, date=namespace.end_date)
                        print("End date was successfully updated"
                              f"\nIf you want to see this date enter `pine date show -i {end_date.date_id}`")
                    if namespace.end_date:
                        end_date = TerminalMenu.service.update_checked_date(end_date.date_id, date=namespace.end_date)
                        print("End date was successfully updated"
                              f"\nIf you want to see this date enter pine date show -i {end_date.date_id}.")
            else:
                print("Task doesn't have any dates"
                      "\nIf you want to add date to task enter"
                      f"\n`pine date add -i {task.task_id}` and then date, which you want to add.")

    @staticmethod
    def handle_close_task(namespace):
        task_id = namespace.task_id
        TerminalMenu.service.update_checked_task(task_id=task_id, status="executed")
        print("Task was successfully executed and closed.")

    @staticmethod
    def handle_delete_task(namespace):
        task_id = namespace.task_id
        TerminalMenu.service.delete_checked_task(task_id)
        print("Task was successfully deleted")

    # endregion

    # region Group: create, show, update and delete group;
    @staticmethod
    def handle_group(namespace, *args, **kwargs):
        TerminalMenu.handle_command(TerminalMenu.group_handlers, namespace.second_command, namespace=namespace, *args,
                                    **kwargs)

    @staticmethod
    def handle_create_group(namespace):
        creator_id = TerminalMenu.service.current_user_id

        group = Group(creator_id=creator_id, title=namespace.title)
        group = TerminalMenu.service.add_checked_group(group)
        print(f"Group was successfully created with id {group.group_id}"
              f"\nIf you want to see this group enter `pine group show -i {group.group_id}`")

    @staticmethod
    def handle_show_group(namespace):
        group_id = namespace.group_id
        group_view = TerminalMenu.service.view_checked_group(group_id)
        print("      Group")
        Printers.show_group(group_view.group)
        if group_view.group_tasks:
            print("       Tasks")
            for task in group_view.group_tasks:
                Printers.show_task(task)
        else:
            print("Group doesn't have any tasks")

    @staticmethod
    def handle_update_group(namespace):
        group_id = namespace.group_id
        group = TerminalMenu.service.update_checked_group(group_id, namespace.title)
        print("Group was successfully updated"
              f"\nIf you want to see this group enter `pine group show -i {group.group_id}`")

    @staticmethod
    def handle_delete_group(namespace):
        group_id = namespace.group_id
        TerminalMenu.service.delete_checked_group_by_id(group_id)
        print("Group was successfully deleted")

    # endregion

    # region Plan: create, show, update and delete;
    @staticmethod
    def handle_plan(namespace, *args, **kwargs):
        TerminalMenu.handle_command(TerminalMenu.plan_handlers, namespace.second_command, namespace=namespace, *args,
                                    **kwargs)

    @staticmethod
    def handle_create_plan(namespace):
        creator_id = TerminalMenu.service.current_user_id
        if not TerminalMenu._is_end_reason_valid(namespace):
            return
        if namespace.count:
            namespace.end_reason = PlanEndReason.AFTER_NUMBER_OF_TIMES.value
        elif namespace.end_date:
            namespace.end_reason = PlanEndReason.AFTER_DATE.value
        else:
            namespace.end_reason = PlanEndReason.NEVER.value

        task = Task(title=namespace.title, creator_id=creator_id,
                    task_type=TaskType.PLAN.value, status=TaskStatus.DEFINED.value,
                    description=namespace.description, priority=namespace.priority,
                    subtask_dependency=namespace.sub_dependent)

        plan = Plan(start_date=namespace.start_date, repeat_style=namespace.repeat_style,
                    interval_counter=namespace.interval_counter,
                    end_reason=namespace.end_reason, end_date=namespace.end_date,
                    count=namespace.count)

        task, plan = TerminalMenu.service.add_checked_plan(task, plan)

        print(f"Plan was successfully created with id {plan.plan_id}"
              f"\nIf you want to see this plan enter `pine plan show -i {plan.plan_id}`")

    @staticmethod
    def _is_end_reason_valid(namespace):
        if namespace.count and namespace.end_date:
            print("You can not add a simultaneous completion of the plan after a certain date"
                  "\nand after the number of repetitions. Choose one.")
            return False
        if (namespace.end_date and
                namespace.end_reason == 'number'):
            print("You can not specify the `end_date` and the 'end_reason' `number` at the same time."
                  "\nChoose one thing.")
            return False
        if (namespace.count and
                namespace.end_reason == 'date'):
            print("You can not specify the `count` and the 'end_reason' `date` at the same time."
                  "\nChoose one thing.")
            return False
        return True

    @staticmethod
    def handle_show_plan(namespace):
        plan_id = namespace.plan_id

        plan_view = TerminalMenu.service.view_checked_plan(plan_id)
        print("      Plan")
        Printers.show_plan(plan_view.plan)
        print("      Template Task")
        Printers.show_task(plan_view.plan_tasks)

    @staticmethod
    def handle_update_plan(namespace):
        plan_id = namespace.plan_id

        plan = TerminalMenu.service.update_checked_plan(plan_id,
                                                        start_date=namespace.start_date,
                                                        repeat_style=namespace.repeat_style,
                                                        interval_counter=namespace.interval_counter,
                                                        end_reason=namespace.end_reason,

                                                        end_date=namespace.end_date,
                                                        count=namespace.count,
                                                        active=namespace.active)

        print(f"""Plan with id {plan.plan_id} was successfully update.
                  \nIf you want to see it enter `pine plan show -i {plan.plan_id}`""")

    @staticmethod
    def handle_delete_plan(namespace):
        plan_id = namespace.plan_id

        plan = TerminalMenu.service.get_checked_plan_by_id(plan_id)
        TerminalMenu.service.delete_checked_task(plan.task_id)
        TerminalMenu.service.delete_checked_plan(plan_id)
        print("Plan was successfully deleted")

    # endregion

    # region Message: show and delete message;
    @staticmethod
    def handle_message(namespace, *args, **kwargs):
        TerminalMenu.handle_command(TerminalMenu.message_handlers,
                                    namespace.second_command, namespace=namespace, *args, **kwargs)

    @staticmethod
    def handle_show_message(namespace):
        message_id = namespace.message_id
        message = TerminalMenu.service.get_checked_message_by_id(message_id)
        print("      Message")
        Printers.show_message(message)

    @staticmethod
    def handle_delete_message(namespace):
        message_id = namespace.message_id
        TerminalMenu.service.delete_checked_message(message_id)
        print("Message was successfully deleted")

    # endregion

    # region Tasks: show tasks;
    @staticmethod
    def handle_show_tasks(namespace):
        details = namespace.details
        tasks_view = TerminalMenu.service.view_all_user_tasks()
        if tasks_view.creator:
            for task in tasks_view.creator:
                if details:
                    Printers.show_task(task)
                    print("\n")
                else:
                    Printers.show_task(task, compact=True)

        if tasks_view.executor:
            print("       Executor")
            for task in tasks_view.executor:
                if details:
                    Printers.show_task(task)
                    print("\n")
                else:
                    Printers.show_task(task)

        if not tasks_view.executor and not tasks_view.creator:
            print("User doesn't have any tasks")

    # endregion

    # region Subtasks: show aubtasks;
    @staticmethod
    def handle_show_subtasks(namespace):
        task_id = namespace.task_id
        subtasks_view = TerminalMenu.service.view_all_task_subtasks(task_id)

        def show_sub_tasks(sub_tasks_view, level):
            task = sub_tasks_view.task
            relation_type = sub_tasks_view.relation_type
            relation_id = sub_tasks_view.relation_id
            print(("{tabs} - {r_id} {type} - {id} - {title} - {status}|"
                   .format(tabs="".join(["\t " for _ in range(0, level)]), type=relation_type, id=task.task_id,
                           title=task.title, r_id=relation_id,
                           status=task.status)))
            for sub_task_view in sub_tasks_view.subtask_views_tree:
                show_sub_tasks(sub_task_view, level + 1)

        show_sub_tasks(subtasks_view, 0)

    # endregion

    # region Groups: show groups;
    @staticmethod
    def handle_show_groups(namespace):
        groups_view = TerminalMenu.service.view_all_checked_group()
        if groups_view.user_groups:
            print("       User Groups")
            for group in groups_view.user_groups:
                Printers.show_group(group, compact=True)
        else:
            print(" User doesn't have any groups")

    # endregion

    # region Messages: show messages;
    @staticmethod
    def handle_show_messages(namespace):
        messages_view = TerminalMenu.service.view_checked_messages()
        if messages_view.messages:
            print("       Messages")
            for message in messages_view.messages:
                Printers.show_message(message, compact=True)
        else:
            print("User doesn't have any messages")

    # endregion

    # region Users: show users;
    @staticmethod
    def handle_show_users(namespace):
        users = TerminalMenu.service.get_all_checked_users()
        for user in users:
            Printers.show_user(user)

    # endregion

    # region Date: create, show, update and delete date;
    @staticmethod
    def handle_date(namespace, *args, **kwargs):
        TerminalMenu.handle_command(TerminalMenu.date_handlers, namespace.second_command,
                                    namespace=namespace, *args, **kwargs)

    @staticmethod
    def handle_create_date(namespace):
        date = TaskDate(task_id=namespace.task_id, date_type=namespace.type,
                        date=namespace.date)
        date = TerminalMenu.service.add_checked_date(date)
        print(f"Date was successfully created with id {date.date_id}"
              f"\nIf you want to see this date enter `pine date show -i {date.date_id}`")

    @staticmethod
    def handle_show_date(namespace):
        date_id = namespace.date_id
        date_view = TerminalMenu.service.view_checked_date(date_id)
        print("      Date")
        Printers.show_date(date_view.date)
        print("      Task")
        Printers.show_task(date_view.task)

    @staticmethod
    def handle_update_date(namespace):
        date_id = namespace.date_id
        date = TerminalMenu.service.update_checked_date(date_id, date=namespace.date)
        print(f"Date with id {date.date_id} was successfully updated")

    @staticmethod
    def handle_delete_date(namespace):
        date_id = namespace.date_id
        TerminalMenu.service.delete_checked_date_by_id(date_id)
        print("Date was successfully deleted")

    # endregion

    # region Connections: task-to-task, group-to-task, executor-to-task relations;

    # region TaskToTaskRelation: create, show, update and delete task-to-task relation;
    @staticmethod
    def handle_task_to_task_relation(namespace, *args, **kwargs):
        TerminalMenu.handle_command(TerminalMenu.task_to_task_relation_handlers, namespace.second_command,
                                    namespace=namespace, *args, **kwargs)

    @staticmethod
    def handle_create_task_to_task_relation(namespace):
        relation = TaskToTaskRelation(parent_task_id=namespace.parent_id,
                                      subtask_id=namespace.child_id,
                                      relation_type=namespace.relation_style)
        relation = TerminalMenu.service.add_checked_task_to_task_relation(relation)
        print(f"Task-to-task relation was successfully created with id {relation.relation_id}"
              f"\nIf you want to see it, enter `pine task-to-task show -i {relation.relation_id}`")

    @staticmethod
    def handle_show_task_to_task_relation(namespace):
        relation_id = namespace.relation_id
        relation = TerminalMenu.service.get_checked_task_to_task_relation_by_id(relation_id)
        print("      Task to Task relation")
        Printers.show_task_to_task_relation(relation)

    @staticmethod
    def handle_update_task_to_task_relation(namespace):
        relation_id = namespace.relation_id
        relation = TerminalMenu.service.update_checked_task_to_task_relation(relation_id,
                                                                             relation_type=namespace.relation_style)
        print(f"Task-to-task relation with id {relation.relation_id} was successfully updated")

    @staticmethod
    def handle_delete_task_to_task_relation(namespace):
        parent_id = namespace.parent_id
        child_id = namespace.child_id
        TerminalMenu.service.delete_checked_task_to_task_relation(parent_id, child_id)
        print("Task-to-task relation was successfully deleted")

    # endregion

    # region GroupToTaskRelation: create and delete group-to-task relation;
    @staticmethod
    def handle_group_to_task_relation(namespace, *args, **kwargs):
        TerminalMenu.handle_command(TerminalMenu.group_to_task_relation_handlers, namespace.second_command,
                                    namespace=namespace, *args, **kwargs)

    @staticmethod
    def handle_create_group_to_task_relation(namespace):
        relation = GroupToTaskRelation(task_id=namespace.task_id,
                                       group_id=namespace.group_id)
        relation = TerminalMenu.service.add_checked_group_to_task_relation(relation)
        print(f"Group-to-task relation was successfully created with id {relation.relation_id}"
              f"\nIf you want to see it, enter `pine group-to-task show -i {relation.relation_id}`")

    @staticmethod
    def handle_delete_group_to_task_relation(namespace):
        task_id = namespace.task_id
        group_id = namespace.group_id
        TerminalMenu.service.delete_checked_group_to_task_relation(task_id, group_id)
        print("Group-to-task relation was successfully deleted")

    # endregion

    # region ExecutorToTaskRelation: create and delete executor-to-task relation;
    @staticmethod
    def handle_executor_to_task_relation(namespace, *args, **kwargs):
        TerminalMenu.handle_command(TerminalMenu.executor_to_task_relation_handlers, namespace.second_command,
                                    namespace=namespace, *args, **kwargs)

    @staticmethod
    def handle_create_executor_to_task_relation(namespace):
        relation = ExecutorToTaskRelation(executor_id=namespace.executor_id,
                                          task_id=namespace.task_id)
        relation = TerminalMenu.service.add_checked_executor_to_task_relation(relation)
        print(f"Executor-to-task relation was successfully created with id {relation.relation_id}"
              f"\nIf you want to see it, enter `pine executor-to-task show -i {relation.relation_id}`")

    @staticmethod
    def handle_delete_executor_to_task_relation(namespace):
        task_id = namespace.task_id
        executor_id = namespace.executor_id
        TerminalMenu.service.delete_checked_executor_to_task_relation(task_id, executor_id)
        print("Executor-to-task relation was successfully deleted")

    # endregion

    # endregion :

    # region Alert: create, show, update and delete alert;
    @staticmethod
    def handle_alert(namespace, *args, **kwargs):
        TerminalMenu.handle_command(TerminalMenu.alert_handlers, namespace.second_command,
                                    namespace=namespace, *args, **kwargs)

    @staticmethod
    def handle_create_alert(namespace):
        alert = Alert(task_id=namespace.task_id, start_date=namespace.start_date,
                      date_interval=namespace.days_interval)
        alert = TerminalMenu.service.add_checked_alert(alert)
        print(f"Alert was successfully created with id {alert.alert_id}")

    @staticmethod
    def handle_show_alert(namespace):
        alert_id = namespace.alert_id
        alert_view = TerminalMenu.service.view_checked_alert(alert_id)
        print("      Alert")
        Printers.show_alert(alert_view.alert)
        print("      Alert Task")
        Printers.show_task(alert_view.task)

    @staticmethod
    def handle_update_alert(namespace):
        alert_id = namespace.alert_id
        alert = TerminalMenu.service.update_checked_alert(alert_id,
                                                          start_date=namespace.start_date,
                                                          date_interval=namespace.days_interval,
                                                          active=namespace.active)
        print(f"Alert with id {alert.alert_id} was successfully updated")

    @staticmethod
    def handle_delete_alert(namespace):
        alert_id = namespace.alert_id
        TerminalMenu.service.delete_checked_alert(alert_id)
        print("Alert was successfully deleted")

    # endregion


# region Commands
TerminalMenu.handlers = {
    Commands.TASK: TerminalMenu.handle_task,
    Commands.GROUP: TerminalMenu.handle_group,
    Commands.PLAN: TerminalMenu.handle_plan,
    Commands.MESSAGE: TerminalMenu.handle_message,
    Commands.TASKS: TerminalMenu.handle_show_tasks,
    Commands.SUBTASKS: TerminalMenu.handle_show_subtasks,
    Commands.GROUPS: TerminalMenu.handle_show_groups,
    Commands.MESSAGES: TerminalMenu.handle_show_messages,
    Commands.USERS: TerminalMenu.handle_show_users,
    Commands.DATE: TerminalMenu.handle_date,
    Commands.ALERT: TerminalMenu.handle_alert,
    Commands.TASK_TO_SUBTASK: TerminalMenu.handle_task_to_task_relation,
    Commands.GROUP_TO_TASK: TerminalMenu.handle_group_to_task_relation,
    Commands.EXECUTOR_TO_TASK: TerminalMenu.handle_executor_to_task_relation
}

TerminalMenu.task_handlers = {
    Commands.ADD: TerminalMenu.handle_create_task,
    Commands.SHOW: TerminalMenu.handle_show_task,
    Commands.UPDATE: TerminalMenu.handle_update_task,
    Commands.CLOSE: TerminalMenu.handle_close_task,
    Commands.DELETE: TerminalMenu.handle_delete_task
}

TerminalMenu.group_handlers = {
    Commands.ADD: TerminalMenu.handle_create_group,
    Commands.SHOW: TerminalMenu.handle_show_group,
    Commands.UPDATE: TerminalMenu.handle_update_group,
    Commands.DELETE: TerminalMenu.handle_delete_group
}

TerminalMenu.plan_handlers = {
    Commands.ADD: TerminalMenu.handle_create_plan,
    Commands.UPDATE: TerminalMenu.handle_update_plan,
    Commands.SHOW: TerminalMenu.handle_show_plan,
    Commands.DELETE: TerminalMenu.handle_delete_plan,
}

TerminalMenu.message_handlers = {
    Commands.SHOW: TerminalMenu.handle_show_message,
    Commands.DELETE: TerminalMenu.handle_delete_message
}

TerminalMenu.date_handlers = {
    Commands.ADD: TerminalMenu.handle_create_date,
    Commands.SHOW: TerminalMenu.handle_show_date,
    Commands.UPDATE: TerminalMenu.handle_update_date,
    Commands.DELETE: TerminalMenu.handle_delete_date,
}

TerminalMenu.alert_handlers = {
    Commands.ADD: TerminalMenu.handle_create_alert,
    Commands.SHOW: TerminalMenu.handle_show_alert,
    Commands.UPDATE: TerminalMenu.handle_update_alert,
    Commands.DELETE: TerminalMenu.handle_delete_alert,
}

TerminalMenu.task_to_task_relation_handlers = {
    Commands.ADD: TerminalMenu.handle_create_task_to_task_relation,
    Commands.SHOW: TerminalMenu.handle_show_task_to_task_relation,
    Commands.UPDATE: TerminalMenu.handle_update_task_to_task_relation,
    Commands.DELETE: TerminalMenu.handle_delete_task_to_task_relation,
}

TerminalMenu.group_to_task_relation_handlers = {
    Commands.ADD: TerminalMenu.handle_create_group_to_task_relation,
    Commands.DELETE: TerminalMenu.handle_delete_group_to_task_relation,
}

TerminalMenu.executor_to_task_relation_handlers = {
    Commands.ADD: TerminalMenu.handle_create_executor_to_task_relation,
    Commands.DELETE: TerminalMenu.handle_delete_executor_to_task_relation,
}
# endregion
