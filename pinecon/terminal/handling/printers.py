# coding=utf-8
""" This module contains a class for the beautiful presentation of data for the user.

    Classes:
    ----------------
    Printers - this class contains functions that provide nice output to the user,
               when he enters the show command;

"""

from pinecon.library.models.model.task import Task
from pinecon.library.models.model.group import Group
from pinecon.library.models.model.plan import Plan, PlanEndReason
from pinecon.library.models.model.message import Message
from pinecon.library.models.model.task_date import TaskDate
from pinecon.library.models.model.task_to_task import TaskToTaskRelation
from pinecon.library.models.model.group_to_task import GroupToTaskRelation
from pinecon.library.models.model.executor_to_task import ExecutorToTaskRelation
from pinecon.library.models.model.alert import Alert
from pinecon.library.models.model.user import User


class Printers:
    """ Contains functions that provide nice output to the user,
        when he enters the show command.

            Public Methods:
            ----------------
                * show_task(task: model.Task, compact=False)
                    - provide nice output to the user, when he enters the 'task show` command;
                * show_group(group: model.Group, compact=False)
                    - provide nice output to the user, when he enters the 'group show` command;
                * show_plan(plan: model.Plan)
                    - provide nice output to the user, when he enters the 'plan show` command;
                * show_message(message: model.Message, compact=False)
                    - provide nice output to the user, when he enters the 'message show` command;
                * show_date(date: model.Date, compact=False)
                    - provide nice output to the user, when he enters the 'date show` command;
                * show_task_to_task_relation(relation: model.TaskToTaskRelation)
                    - provide nice output to the user, when he enters the 'task-to-task show` command;
                * show_group_to_task_relation(relation: model.GroupToTaskRelation)
                    - provide nice output to the user, when he enters the 'group-to-task show` command;
                * show_executor_to_task_relation(relation: model.ExecutorToTaskRelation)
                    - provide nice output to the user, when he enters the `executor-to-task show` command;
                * show_alert(alert: model.Alert, compact=False)
                    - provide nice output to the user, when he enters the `alert show` command;
                * show_user(user: model.User)
                    - provide nice output to the user, when he enters the `user show` command;

    """
    @staticmethod
    def show_task(task: Task, compact=False):
        """ Provide nice output to the user, when he enters the 'task show` command. """
        if compact:
            print(f' {task.task_id} - {task.title}')
        else:
            print(f" Title: {' '*29} | {task.title}\n"
                  f" Task id: {' '*27} | {task.task_id}\n"
                  f" Type: {' '*30} | {task.task_type}\n"
                  f" Description: {' '*23} | {task.description}\n"
                  f" Created on: {' '*24} | {task.creation_date}\n"
                  f" Priority: {' '*26} | {task.priority}\n"
                  f" Status: {' '*28} | {task.status}\n"
                  f" Close, if last subtask was executed: | {task.subtask_dependency}")

    @staticmethod
    def show_group(group: Group, compact=False):
        """ Provide nice output to the user, when he enters the 'group show` command. """
        if compact:
            print(f" {group.group_id} - {group.title}")
        else:
            print(f" Group id: {' '*2} | {group.group_id}\n"
                  f" Title: {' '*5} | {group.title}")

    @staticmethod
    def show_plan(plan: Plan):
        """ Provide nice output to the user, when he enters the 'plan show` command. """
        print(f" Plan id: {' '*13} | {plan.plan_id}\n"
              f" Active: {' '*14} | {plan.active}\n"
              f" Template task id: {' '*4} | {plan.task_id}\n"
              f" Next creation date: {' '*2} | {plan.start_date}\n"
              f" Repeated times: {' '*6} | {plan.count}\n"
              f" Repetitions type: {' '*4} | {plan.repeat_style}\n"
              f" Interval: {' '*12} | {plan.interval_counter}\n"
              f" End type: {' '*12} | {plan.end_reason}")

        if plan.end_reason == PlanEndReason.AFTER_NUMBER_OF_TIMES:
            print(f" Times to repeat: {plan.count}")
        elif plan.end_reason == PlanEndReason.AFTER_DATE:
            print(f" Will be turned off after: {plan.end_date}")

    @staticmethod
    def show_message(message: Message, compact=False):
        """ Provide nice output to the user, when he enters the 'message show` command. """
        if compact:
            print(f" {message.message_id} - {message.title} ")
        else:
            print(f" Title: {' '*4} | {message.title}\n"
                  f" Message: {' '*2} | {message.text}\n"
                  f" Date: {' '*3} | {message.date}\n"
                  f" Time: {' '*3} | {message.time}")

    @staticmethod
    def show_date(date: TaskDate, compact=False):
        """ Provide nice output to the user, when he enters the 'date show` command. """
        if compact:
            print(f" {date.date_id} - {date.date_type} {date.date}")
        else:
            print(f" Date id: {' '*2} | {date.date_id}\n"
                  f" Type: {' '*5} | {date.date_type}\n"
                  f" Date: {' '*5} | {date.date}\n")

    @staticmethod
    def show_task_to_task_relation(relation: TaskToTaskRelation):
        """ Provide nice output to the user, when he enters the 'task-to-task show` command. """
        print(f" Relation id: {' '*5} | {relation.relation_id}\n"
              f" Parent task id: {' '*2} | {relation.parent_task_id}\n"
              f" Child task id: {' '*3} | {relation.subtask_id}\n"
              f" Relation type: {' '*3} | {relation.relation_type}")

    @staticmethod
    def show_group_to_task_relation(relation: GroupToTaskRelation):
        """ Provide nice output to the user, when he enters the 'group-to-task show` command. """
        print(f" Relation id: {' '*2} | {relation.relation_id}\n"
              f" Task id: {' '*6} | {relation.task_id}\n"
              f" Group id: {' '*5} | {relation.group_id}")

    @staticmethod
    def show_executor_to_task_relation(relation: ExecutorToTaskRelation):
        """ Provide nice output to the user, when he enters the `executor-to-task show` command. """
        print(f" Executor id: {' '*2} | {relation.executor_id}\n"
              f" Task id: {' '*6} | {relation.task_id}")

    @staticmethod
    def show_alert(alert: Alert, compact=False):
        """ Provide nice output to the user, when he enters the `alert show` command. """
        if compact:
            print(f" {alert.alert_id} - (task id: {alert.task_id}, alert date: {alert.start_date},"
                  f" days interval: {alert.date_interval}, active: {alert.active})")
        else:
            print(f" Alert id: {' '*10} | {alert.alert_id}\n"
                  f" Task id: {' '*11} | {alert.task_id}\n"
                  f" Next alert date: {' '*3} | {alert.start_date}\n"
                  f" Interval in days: {' '*2} | {alert.date_interval}\n"
                  f" Active: {' '*12} | {alert.active}")

    @staticmethod
    def show_user(user: User):
        """ Provide nice output to the user, when he enters the `user show` command. """
        print(f" User ID: {' '*3} | {user.user_id}\n"
              f" Username: {' '*2} | {user.name}")
