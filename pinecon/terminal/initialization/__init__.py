"""  This package contains modules related to the primary processing of the result to the user:
     simple type checks and etc. This module also contains helps and the interface with which the user interacts.

     Modules:
     ----------------
     * commands -  contains a class that is responsible for string representation of commands in the parser;
     * parser - processes the data entered by the user;
     * validation - test the types that are passed to the parser;
"""