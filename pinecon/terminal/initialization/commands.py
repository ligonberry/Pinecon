""" The module contains a class that is responsible for string representation of commands in the parser. """


class Commands:
    """
    Contains commands in a string representation to make it easier to work with the parser.
    The commands listed are the functionality of the application.
    """
    FIRST = "first_command"
    SECOND = "second_command"

    TASK = "task"
    GROUP = "group"
    PLAN = "plan"

    STATUS = "status"

    MESSAGE = "message"

    TASKS = "tasks"
    SUBTASKS = "subtasks"
    GROUPS = "groups"

    MESSAGES = "messages"
    USERS = "users"

    DATE = "date"

    ALERT = "alert"

    TASK_TO_SUBTASK = "task-to-task"
    GROUP_TO_TASK = "group-to-task"
    EXECUTOR_TO_TASK = "executor-to-task"

    INITIALISATION = "init"
    LOGIN = 'login'

    ADD = "add"
    SHOW = "show"
    CLOSE = "close"
    DELETE = "delete"
    UPDATE = "update"
