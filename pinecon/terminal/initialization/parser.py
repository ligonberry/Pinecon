""" This module processes the data entered by the user.

    Classes:
    ----------------
        MultilineFormatter(argparse.HelpFormatter)
            - basically inherits the behavior of class HelpFormatter.
              Overrides the methods to display help and description.

     Public Methods:
    ----------------
        get_parsed_data()
            - processes data entered by the user in the following format 'pine object command params';
"""
import argparse

from pinecon.library.models import helpers
from pinecon.library.models.model.task import TaskPriority, TaskStatus, TASK_PRIORITY_TUPLE, TASK_STATUS_TUPLE
from pinecon.library.models.model.plan import PLAN_REPETITION_STYLE_TUPLE, PLAN_END_REASON_TUPLE
from pinecon.library.models.model.task_to_task import TaskRelation, TASK_RELATIONS_TUPLE
from pinecon.library.models.model.task_date import TaskDateType, TASK_DATE_TYPE_TUPLE


from pinecon.terminal.initialization import validation, commands


class MultilineFormatter(argparse.HelpFormatter):
    """
    Basically inherits the behavior of class HelpFormatter.
    Overrides the methods to display help and description.
    """
    def _fill_text(self, text, width, indent):
        return ''.join([indent + line.strip(' ') for line in text.splitlines(True)])

    def _split_lines(self, text, width):
        return [line.strip() for line in text.splitlines()]


def _positive_int(i):
    return validation.is_positive_integer(i)


def _date(s):
    return validation.is_valid_date(s)


def _future_date(s):
    return validation.is_valid_future_date(s)


def get_parsed_data():
    """ Processes data entered by the user in the following format 'pine object command params'.

        Returns
        -----------
        parser_.parse_args(): named tuple
            Namespace(first_command=object, second_command='command' [, ...]).
            Objects: task, group, plan, message, alert, user, ...
            Params parameters are specific to each object. Some of them are required (as user name)
    """
    parser_ = argparse.ArgumentParser(prog='Pinecon',
                                      usage='pine ',
                                      description=("""Memory like a sieve? Now there’s no need to remember all 
                                                      those things that you have to do, because Pinecon will do that for you! 
                                                      The application will help you to remind both about important events (birthdays, holidays) 
                                                      and about daily routine. You can create simple tasks and plans, group them 
                                                      and give them to other users for execution. """),
                                      epilog="(c) Darya Litvinchuk (Ligonberry) 2018",
                                      formatter_class=MultilineFormatter)

    # action command subparsers to invoke sub-commands
    # (examples : pine task add, pine group delete ... )
    subparsers = parser_.add_subparsers(dest=commands.Commands.FIRST,
                                        title="""Characterizes the essence of the project : objectives, 
                                                 plans, etc. the user wants to work""",

                                        description="""This commands should go as first parameter 
                                                       specifying essence, with which the user wants to work""")

    task_subparser = subparsers.add_parser(commands.Commands.TASK, formatter_class=MultilineFormatter,
                                           help="Type to work with task")
    _task_initialize_subparser(task_subparser)

    group_subparser = subparsers.add_parser(commands.Commands.GROUP, formatter_class=MultilineFormatter,
                                            help="Type to work with group")
    _group_initialize_subparser(group_subparser)

    plan_subparser = subparsers.add_parser(commands.Commands.PLAN, formatter_class=MultilineFormatter,
                                           help="Type to work with plan")
    _plan_initialize_subparser(plan_subparser)

    message_subparser = subparsers.add_parser(commands.Commands.MESSAGE, formatter_class=MultilineFormatter,
                                              help="Type to work with message")
    _message_initialize_subparser(message_subparser)

    tasks_subparser = subparsers.add_parser(commands.Commands.TASKS, formatter_class=MultilineFormatter,
                                            help="Type to work with users tasks")
    _tasks_initialize_subparser(tasks_subparser)

    subtasks_subparser = subparsers.add_parser(commands.Commands.SUBTASKS, formatter_class=MultilineFormatter,
                                               help="Type to work with task subtasks")
    _subtasks_initialize_subparser(subtasks_subparser)

    groups_subparser = subparsers.add_parser(commands.Commands.GROUPS, formatter_class=MultilineFormatter,
                                             help="Type to work with user groups")
    _groups_initialize_subparser(groups_subparser)

    messages_subparser = subparsers.add_parser(commands.Commands.MESSAGES, formatter_class=MultilineFormatter,
                                               help="Type to work with user messages ")
    _messages_initialize_subparser(messages_subparser)

    users_subparser = subparsers.add_parser(commands.Commands.USERS, formatter_class=MultilineFormatter,
                                            help="Type to work with users")
    _users_initialize_subparser(users_subparser)

    date_subparser = subparsers.add_parser(commands.Commands.DATE, formatter_class=MultilineFormatter,
                                           help="Type to work with dates")
    _date_initialize_subparser(date_subparser)

    task_to_task_subparser = subparsers.add_parser(commands.Commands.TASK_TO_SUBTASK,
                                                   formatter_class=MultilineFormatter,
                                                   help="Type to work with task and subtask connections")
    task_to_task_initialize_subparser(task_to_task_subparser)

    group_to_task_subparser = subparsers.add_parser(commands.Commands.GROUP_TO_TASK, formatter_class=MultilineFormatter,
                                                    help="Type to work with group and task connections")
    _group_to_task_initialize_parser(group_to_task_subparser)

    executor_to_task_subparser = subparsers.add_parser(commands.Commands.EXECUTOR_TO_TASK,
                                                       formatter_class=MultilineFormatter,
                                                       help="Type to work with executor and task connections")
    _executor_to_task_initialize_parser(executor_to_task_subparser)

    alert_subparser = subparsers.add_parser(commands.Commands.ALERT, formatter_class=MultilineFormatter,
                                            help="Type to work with alerts")
    _alert_initialize_subparser(alert_subparser)

    init_subparser = subparsers.add_parser(commands.Commands.INITIALISATION, formatter_class=MultilineFormatter,
                                           help="Initial command to set up database.")
    _init_initialize_subparser(init_subparser)

    login_subparser = subparsers.add_parser(commands.Commands.LOGIN, formatter_class=MultilineFormatter,
                                            help="Login user.")
    _login_initialize_subparser(login_subparser)

    return parser_.parse_args()


# region Task parser : initialize task subparser; create, show, delete, update, execute and close task parser;
def _task_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions",
                                        description="""Task is core object, you can add, show, 
                                                       delete and update task""")
    subparsers.required = True

    task_create_parser = subparsers.add_parser(commands.Commands.ADD, formatter_class=MultilineFormatter,
                                               help="""Create task. 
                                                        Title - required param, 
                                                        another - option.""")

    _task_initialize_create_parser(task_create_parser)

    task_show_parser = subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                                             help="Show user's task giving its id")
    _task_initialize_show_parser(task_show_parser)

    task_close_parser = subparsers.add_parser(commands.Commands.CLOSE, formatter_class=MultilineFormatter,
                                              help="Close user task")
    _task_initialize_close_parser(task_close_parser)

    task_delete_parser = subparsers.add_parser(commands.Commands.DELETE, formatter_class=MultilineFormatter,
                                               help="""Delete task and all its relations (subtask,
                                               user-executor) and object connected with it (dates,
                                               notifications, reminders)""")
    _task_initialize_delete_parser(task_delete_parser)

    task_update_parser = subparsers.add_parser(commands.Commands.UPDATE, formatter_class=MultilineFormatter,
                                               help="Choose id of the task you want to update")
    _task_initialize_update_parser(task_update_parser)


def _task_initialize_create_parser(parser_):
    parser_.add_argument("-t", "--title", required=True, type=str,
                         help="""You must add title of the task. 
                                 This is a required parameter.
                                 Example: pine task add -t 'feed cat' 
                                 """)
    parser_.add_argument("-d", "--description", type=str,
                         help="""You can add description of the task.
                                 Example: pine task add -t 'feed cat' -d 'Give 50 gr food' 
                                 """)
    parser_.add_argument("-p", "--priority", choices=list(TASK_PRIORITY_TUPLE),
                         default=TaskPriority.MID.value,
                         help="""You can add task priority. 
                                 Example : pine task add -t 'feed cat' -p 'mid' 
                                  """)
    parser_.add_argument('-sd', "--sub-dependent", choices=[helpers.NO, helpers.YES], default=helpers.NO,
                         help="""If you choose 'yes' task will be completed automatically. 
                                 If choose 'no' - it will not completed automatically.
                                 So, don't forget close task if last sub task was executed!
                                 Example : pine task add -t 'feed cat' -sd 'yes' 
                                 """)
    parser_.add_argument("--date", help="""You can add date for task. Enter date in format DD-MM-YYYY.
                                           Example : pine task add -t 'feed cat' --date '28-05-2018' """,
                         type=_date)
    parser_.add_argument('-start', "--start-date", help="""You can add start date for task. 
                                                           Enter date in format DD-MM-YYYY.
                                                           Example: pine task add -t 'feed cat' -start '28-05-2018' """,
                         type=_date)
    parser_.add_argument('-end', "--end-date", help="""You can add end date for task. 
                                                      Enter date in format DD-MM-YYYY.
                                                      Example: pine task add -t 'feed cat' -end '28-06-2018' """,
                         type=_date)


def _task_initialize_show_parser(parser_):
    parser_.add_argument("-i", "--task-id", required=True, type=_positive_int,
                         help="""Id of task you want to show
                                 This is a required parameter.
                                 Example: pine task show -i 1
                                 """)


def _task_initialize_close_parser(parser_):
    parser_.add_argument("-i", "--task-id", required=True, type=_positive_int,
                         help="""Id of task you want to close.
                                 This is a required parameter.
                                 Example: pine task close -i 1
                                 """)


def _task_initialize_delete_parser(parser_):
    parser_.add_argument("-i", "--task-id", required=True, type=_positive_int,
                         help="""Id of task you want to delete.
                                 This is a required parameter.
                                 Example: pine task delete -i 1
                                 """)


def _task_initialize_update_parser(parser_):
    parser_.add_argument("-i", "--task-id", required=True, type=_positive_int,
                         help="""Id of task you want to update.
                                 This is a required parameter.
                                 Example: pine task update -i 1' 
                                 """)
    parser_.add_argument("-t", "--title", type=str, help=""" Title of the task.
                                                             Example: pine task update -i 1 -t 'feed dog' 
                                                             """)
    parser_.add_argument("-d", "--description", type=str,
                         help="""Brief description of the task.
                                 Example: pine task update -i 1 -d 'Give the dog canned' 
                                 """)
    parser_.add_argument("-p", "--priority", choices=list(TASK_PRIORITY_TUPLE),
                         help="""Task priority. Example: pine task update -i 1 -p 'low' """)
    parser_.add_argument("-sd", "--sub-dependent", choices=[helpers.NO, helpers.YES], default=helpers.NO,
                         help="""If you choose 'yes' task will be completed automatically. 
                                 If choose 'no' - it will not completed automatically.
                                 So, don't forget close task if last sub task was executed!
                                 Example: pine task update -i 1 -sd 'yes' 
                                 """)
    parser_.add_argument("-s", "--status",
                         choices=list(TASK_STATUS_TUPLE),
                         default=TaskStatus.CREATED.value,
                         help="""Updated task status. 
                                 Example: pine task update -i 1 -s 'work' 
                                 """)
    parser_.add_argument("-date", type=_date,
                         help="""Date for task. 
                                 Enter date in format DD-MM-YYYY
                                 Example: pine task update -i 1 -date '28-06-2018' 
                                 """)

    parser_.add_argument('-start', "--start-date", type=_date,
                         help="""Start date for this task. 
                                 Enter date in format DD-MM-YYYY.
                                 Example: pine task update -i 1 -start '28-06-2018' 
                                 """)

    parser_.add_argument('-end', "--end-date", type=_date,
                         help="""End date for this task. 
                                 Enter date in format DD-MM-YYYY
                                 Example: pine task update -i 1 -end '30-06-2018' 
                                 """)


def _initialize_execute_task_parser(parser_):
    parser_.add_argument("-i", "--task-id", required=True, type=_positive_int,
                         help="""Id of task you want to execute
                                 This is a required parameter.
                                 Example: pine task execute -i 1
                                 """)


def _initialize_close_task_parser(parser_):
    parser_.add_argument("-i", "--task-id", required=True, type=_positive_int,
                         help="""Id of task you want to close.
                                 This is a required parameter.
                                 Example: pine task close -i 1
                                 """)


# endregion

# region Group parser : initialize group subparser; create, show, delete and update group parser;

def _group_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions",
                                        description="""Group unites set of tasks. Group is a core object,
                                                       you can add, show, delete and update groups.
                                                       """)
    subparsers.required = True

    create_group_parser = subparsers.add_parser(commands.Commands.ADD, formatter_class=MultilineFormatter,
                                                help="""Create group. Title - required param.
                                                        Example: pine group add -t 'university' 
                                                        """)
    _group_initialize_create_parser(create_group_parser)

    show_group_parser = subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                                              help="""Show information about group by its id.
                                                      Example: pine group show -i 1 
                                                      """)
    _group_initialize_show_parser(show_group_parser)

    update_group_parser = subparsers.add_parser(commands.Commands.UPDATE, formatter_class=MultilineFormatter,
                                                help="""Enter id such group, you want to update.
                                                        Enter new title of this group.
                                                        Example: pine group update -i 1 -t 'my university' 
                                                        """)
    _group_initialize_update_parser(update_group_parser)

    delete_group_parser = subparsers.add_parser(commands.Commands.DELETE, formatter_class=MultilineFormatter,
                                                help="""Delete group by its id.
                                                        Note: connected task-group relations delete too.
                                                        Example: pine group delete -i 1 
                                                        """)
    _group_initialize_delete_parser(delete_group_parser)


def _group_initialize_create_parser(parser_):
    parser_.add_argument("-t", "--title", required=True, type=str,
                         help="""Title of the group.
                                 This is a required parameter.
                                 Example: pine group add -t 'my_title'
                                 """)


def _group_initialize_show_parser(parser_):
    parser_.add_argument("-i", "--group-id", required=True, type=_positive_int,
                         help="""Id of group you want to see.
                                 This is a required parameter.
                                 Example: pine group show -i 1
                                 """)


def _group_initialize_delete_parser(parser_):
    parser_.add_argument("-i", "--group-id", required=True, type=_positive_int,
                         help="""Id of group you want to delete.
                                 This is a required parameter.
                                 Example: pine group delete -i 1
                                 """)


def _group_initialize_update_parser(parser_):
    parser_.add_argument("-i", "--group-id", required=True, type=_positive_int,
                         help="""Id of group you want to update.
                                This is a required parameter.
                                Example: pine group update -i 1 .........
                                Fool Example: pine group update -i 1 -t 'my_title2'
                                """)
    parser_.add_argument("-t", "--title", required=True, type=str,
                         help="""New group title.
                                This is a required parameter.
                                Example: pine group update -t 'my_title2' .........
                                Fool Example: pine group update -i 1 -t 'my_title2'
                                """)


# endregion

# region Plan parser : initialize plan subparser; create, show, delete and update group parser;
def _plan_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions",
                                        description="""Plan - task, that is created a specified number of times 
                                                       by a template.""")
    subparsers.required = True

    create_plan_parser = subparsers.add_parser(commands.Commands.ADD, formatter_class=MultilineFormatter,
                                               help="Create plan (templating task).")
    _plan_initialize_create_parser(create_plan_parser)

    show_plan_parser = subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                                             help="Show plan and its template task")
    _plan_initialize_show_parser(show_plan_parser)

    delete_plan_parser = subparsers.add_parser(commands.Commands.DELETE, formatter_class=MultilineFormatter,
                                               help="Delete plan and its template task")
    _plan_initialize_delete_parser(delete_plan_parser)

    update_plan_parser = subparsers.add_parser(commands.Commands.UPDATE, formatter_class=MultilineFormatter,
                                               help="Update plan")
    _plan_initialize_update_parser(update_plan_parser)


def _plan_initialize_create_parser(parser_):
    # Plan part
    parser_.add_argument("-start", "--start-date", required=True, type=_future_date,
                         help="""You must add start date for plan.  
                                This is the date when the plan will be created for the first time.
                                This is a required parameter.
                                Enter date in format DD-MM-YYYY.
                                Example: pine plan add -start '12-04-2020' ...... 
                                Fool example: pine plan add -t 'give the cat a pill' 
                                                            -start '12-04-2020' -r 'daily' -n 2 
                                """)
    parser_.add_argument("-end", "--end-date", type=_future_date,
                         help="""You can add end date for plan.
                                After this time plan will be inactive.
                                Enter date in format DD-MM-YYYY.
                                Example: pine plan add -end '24-04-2020' ...... 
                                Fool example: pine plan add -t 'give the cat a pill' 
                                                            -start '12-04-2020' -end '24-04-2020' -r 'daily' -n 2 
                                """)
    parser_.add_argument("-r", "--repeat-style", required=True, choices=list(PLAN_REPETITION_STYLE_TUPLE),
                         help="""You must add type of repetitions.  
                                This is a required parameter.
                                Example: pine plan add -r 'daily' ......  
                                Fool example: pine plan add -t 'give the cat a pill' 
                                                            -start '12-04-2020'  -r 'daily' -n 2 
                                """)
    parser_.add_argument("-n", "--interval-counter", required=True, type=_positive_int,
                         help="""You must add number of basic interval of repetitions style.  
                                 This is a required parameter.
                                 Example: pine plan add -n 2 ...... 
                                 Fool example: pine plan add -t 'give the cat a pill' 
                                                            -start '12-04-2020' -r 'daily' -n 2 
                                 Is's mean, that task will repeat every 2 days.
                                 """)
    parser_.add_argument("-c", "--count", type=_positive_int,
                         help="""You can add a count of repetitions.  
                                After the plan is executed a specified number of times, 
                                it becomes inactive.
                                Example: pine plan add -c 5 ......  
                                Fool example: pine plan add -t 'give the cat a pill' 
                                                            -start '12-04-2020' -r 'daily' -n 2 -c 5 
                                It's mean, that after 5 repetitions this plan will be inactive.
                                """)
    parser_.add_argument("--end-reason", choices=list(PLAN_END_REASON_TUPLE),
                         help="""Specify the type of reason for plan completion.
                                 Example: pine plan add --end-reason 'never' ......
                                 Fool example: pine plan add -t 'give the cat a pill'
                                                         --end-reason 'never'   
                                 """)
    # Task part
    parser_.add_argument("-t", "--title", required=True, type=str,
                         help="""You must add title of the plan. 
                                This is a required parameter.
                                Example: pine plan add -t 'give the cat a pill' ......  
                                Fool example: pine plan add -t 'give the cat a pill' 
                                                            -start '12-04-2020' -r 'daily' -n 2 
                                """)
    parser_.add_argument("-d", "--description", type=str,
                         help="""You can add description of the plan.
                                 Example: pine plan add -t 'give the cat a pill' 
                                                        -d 'give him the antibiotic that's in the fridge' ...... 
                                 Fool example: pine plan add -t 'give the cat a pill' 
                                                            -start '12-04-2020' -r 'daily' -n 2 
                                                            -d 'give him the antibiotic that's in the fridge'           
                                 """)
    parser_.add_argument("-p", "--priority", choices=list(TASK_PRIORITY_TUPLE),
                         default=TaskPriority.MID.value,
                         help="""Plan priority. 
                                Example: pine plan add -p 'low' ...... 
                                Fool example: pine plan add -t 'give the cat a pill' 
                                                            -start '12-04-2020' -r 'daily' -n 2
                                                            -p 'low'
                                """)
    parser_.add_argument("--sub-dependent", choices=[helpers.NO, helpers.YES],
                         default=helpers.NO,
                         help="""If you choose 'yes' task will be completed automatically. 
                                 If choose 'no' - it will not completed automatically.
                                 So, don't forget close task if last sub task was executed!
                                 Example : pine plan add -sd 'yes' ......
                                 Fool example: pine plan add -t 'give the cat a pill' 
                                                            -start '12-04-2020' -r 'daily' -n 2
                                                            -sd 'yes'
                                 """)


def _plan_initialize_show_parser(parser_):
    parser_.add_argument("-i", "--plan-id", required=True, type=_positive_int,
                         help="""Id of plan you want to see.
                                 This is a required parameter.
                                 Example: pine plan show -i 1
                                 """)


def _plan_initialize_delete_parser(parser_):
    parser_.add_argument("-i", "--plan-id", required=True, type=_positive_int,
                         help="""Id of plan you want to delete. 
                                Template task also will be deleted.
                                This is a required parameter.
                                Example: pine plan delete -i 1
                                """)


def _plan_initialize_update_parser(parser_):
    parser_.add_argument("-i", "--plan-id", required=True,
                         help="""Id of plan you want to update.
                                 This is a required parameter.
                                 Example: pine plan update -i 1
                                 """)
    parser_.add_argument("-a", "--active", choices=list([helpers.YES, helpers.NO]),
                         help="""You can activate plan.
                                 Example: pine plan update -a 1
                                 """)
    parser_.add_argument("-start", "--start-date", type=_future_date,
                         help="""This is the date when the plan will be created for the first time.
                                 Enter date in format DD-MM-YYYY.
                                 Example: pine plan update -i 1 -start '14-05-2022'
                                """)
    parser_.add_argument("-r", "--repeat-style", choices=list(PLAN_REPETITION_STYLE_TUPLE),
                         help="""Type of repetitions.  
                                 Example: pine plan update -i 1 -r 'monthly'
                                 """)
    parser_.add_argument("-n", "--interval-counter", type=_positive_int,
                         help="""Number of basic interval of measured units defined in --repeat-style.
                                 Example: pine plan update -i 1 -n 4
                                 """)
    parser_.add_argument("--end-reason", choices=list(PLAN_END_REASON_TUPLE),
                         help="""Specify the type of reason for plan completion.
                                 Example: pine plan update -i 1 --end-reason 'never' 
                                 """)
    parser_.add_argument("-end", "--end-date", type=_future_date,
                         help="""After this time plan will be inactive.
                                 Enter date in format DD-MM-YYYY.
                                 Example: pine plan update -i 1 -end '27-05-2022'
                                 """)
    parser_.add_argument("-c", "--count", type=_positive_int,
                         help="""After the plan is executed a specified number of times, 
                                 it becomes inactive.
                                 Example: pine plan update -i 1 -c 2 
                                 It's mean, that after 2 repetitions this plan will be inactive.
                                 """)


# endregion

# region Message parser: initialize message subparser; show and delete message;
def _message_initialize_subparser(parser):
    subparsers = parser.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions")
    subparsers.required = True

    show_message_parser = subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                                                help="Show message by id. Example: pine message show -i 1")
    _message_initialize_show_parser(show_message_parser)

    delete_message_parser = subparsers.add_parser(commands.Commands.DELETE, formatter_class=MultilineFormatter,
                                                  help="Delete message by id. Example: pine message delete -i 1")
    _message_initialize_delete_parser(delete_message_parser)


def _message_initialize_show_parser(parser):
    parser.add_argument("-i", "--message-id", required=True, type=_positive_int,
                        help="""Id of message you want to see. 
                                This is a required parameter.
                                Example: pine message show -i 1""")


def _message_initialize_delete_parser(parser_):
    parser_.add_argument("-i", "--message-id", required=True, type=_positive_int,
                         help="""Id of message you want to delete. 
                                 This is a required parameter.
                                 Example: pine message delete -i 1""")


# endregion


# region Tasks parser : initialize tasks subparser; show all user tasks;
def _tasks_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions",
                                        description="""Tasks - all tasks of a certain user. 
                                                       You can show all of them.""")
    subparsers.required = True

    tasks_show_parser = subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                                              help="Show all user's tasks.")
    _tasks_initialize_show_parser(tasks_show_parser)


def _tasks_initialize_show_parser(parser_):
    parser_.add_argument("-d", "--details", action="store_const", const=True, default=False,
                         help="""Show full description of task.
                                 This is a required parameter. 
                                 Example: pine tasks show -d""")


# endregion

# region Subtasks parser : initialize subtasks subparser; show all subtasks of the task;
def _subtasks_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions",
                                        description="""Subtasks - all subtasks of a certain task. 
                                                       You can show all of them.""")
    subparsers.required = True

    subtasks_show_parser = subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                                                 help="Show all subtasks of the task.")
    _subtasks_initialize_show_parser(subtasks_show_parser)


def _subtasks_initialize_show_parser(parser_):
    parser_.add_argument("-i", "--task-id", required=True, type=_positive_int,
                         help="""Show subtasks of parent task by id.
                                 This is a required parameter. 
                                 Example: pine subtasks show -i 1""")


# endregion

# region Groups parser : initialize groups subparser; show all user's groups;
def _groups_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions")

    subparsers.required = True

    subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                          help="Show all user groups. Example: pine groups show")


# endregion

# region Messages parser : initialize messages subparser; show all user's messages;
def _messages_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions")

    subparsers.required = True

    subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                          help="Show all user messages. Example: pine messages show")


# endregion

# region Users parser : initialize users subparser; show all user's messages;
def _users_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions")

    subparsers.required = True

    subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                          help="Show all users. Example: pine users show")


# endregion


# region Data parser: initialize data subparser; create, show, delete and update date;
def _date_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions",
                                        description="""Date is connected with task.
                                                       Event - simple date when task should be executed. 
                                                       Start date - start of period. 
                                                       End date - end of period""")
    subparsers.required = True

    create_date_parser = subparsers.add_parser(commands.Commands.ADD, formatter_class=MultilineFormatter,
                                               help="Create date.")
    _date_initialize_create_parser(create_date_parser)

    show_date_parser = subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                                             help="Show task date")
    _date_initialize_show_parser(show_date_parser)

    update_date_parser = subparsers.add_parser(commands.Commands.UPDATE, formatter_class=MultilineFormatter,
                                               help="Update date by task id")
    _date_initialize_update_parser(update_date_parser)

    delete_date_parser = subparsers.add_parser(commands.Commands.DELETE, formatter_class=MultilineFormatter,
                                               help="Delete date(s) connected with task")
    _date_initialize_delete_parser(delete_date_parser)


def _date_initialize_create_parser(parser_):
    parser_.add_argument("-i", "--task-id", required=True, type=_positive_int,
                         help="""Id of task, date of which you want to create.
                                 This is a required parameter.
                                 Example: pine date add -i 1 
                                 """)
    parser_.add_argument("--date", required=True, type=_date,
                         help="""Enter date in format DD-MM-YYYY.
                                 This is a required parameter.
                                 Example : pine date add -i 1 --date '03-01-2019'
                                 """)
    parser_.add_argument("-t", "--type", choices=list(TASK_DATE_TYPE_TUPLE),
                         default=TaskDateType.EVENT.value,
                         help="""You can add type of date.
                                 Event - simple date when task should be executed. 
                                 Start date - start of period. 
                                 End date - end of period.  
                                 Example1 : pine date add -t 'event'   
                                 Example2 : pine date add -t 'start' 
                                 """)


def _date_initialize_show_parser(parser_):
    parser_.add_argument("-i", "--date-id", required=True, type=_positive_int,
                         help="""Id of date you want to show.
                                 This is a required parameter.
                                 Example: pine show -i 1""")


def _date_initialize_delete_parser(parser_):
    parser_.add_argument("-i", "--date-id", required=True, type=_positive_int,
                         help="""Id of date you want to delete.
                                 This is a required parameter.
                                 Example: pine date delete -i 1""")


def _date_initialize_update_parser(parser_):
    parser_.add_argument("-i", "--date-id", required=True, type=_positive_int,
                         help="""Id of date you want to update.
                                 This is a required parameter.
                                 Example: pine date update -i 1
                                 """)
    parser_.add_argument("--date", type=_date,
                         help="""Enter date in format DD-MM-YYYY.
                                 This is a required parameter.
                                 Example: pine date update -i 1 --date '14-05-2019'
                                 """)


# endregion

# region Connections parsers: task to subtask, group to task, executor to task connections;

# region Task to subtask connections parser: create, show, delete and update task-to-task parser
def task_to_task_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions",
                                        description="""Task to task connections describes the 
                                        relationship between two tasks: the parent task and the subtask.
                                        Connection can be blocking (you can't execute parent task without 
                                        executing subtask). 
                                        If tasks are depends and parent task is executed, 
                                        all connected subtasks are execute too. 
                                        If tasks only connected, they connect only logically.""")
    subparsers.required = True

    create_task_to_task_parser = subparsers.add_parser(commands.Commands.ADD, formatter_class=MultilineFormatter,
                                                       help="Create task to task connections")
    _task_to_task_initialize_create_parser(create_task_to_task_parser)

    show_task_to_task_parser = subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                                                     help="Show task to task connection")
    _task_to_task_initialize_show_parser(show_task_to_task_parser)

    update_task_to_task_parser = subparsers.add_parser(commands.Commands.UPDATE, formatter_class=MultilineFormatter,
                                                       help="Update task to task connection")
    _task_to_task_initialize_update_parser(update_task_to_task_parser)

    delete_task_to_task_parser = subparsers.add_parser(commands.Commands.DELETE, formatter_class=MultilineFormatter,
                                                       help="Delete task to task connection")
    _task_to_task_initialize_delete_parser(delete_task_to_task_parser)


def _task_to_task_initialize_create_parser(parser_):
    parser_.add_argument("-pi", "--parent-id", required=True, type=_positive_int,
                         help="""Id of parent task, subtask of which you want to create.
                                 This is a required parameter.
                                 Example: pine task-to-task add -pi 1 .........
                                 Fool Example: pine task-to-task add -pi 1 -ci 2
                                 """)
    parser_.add_argument("-ci", "--child-id", required=True, type=_positive_int,
                         help="""Id of child task, which you want to make a subtask.
                                 This is a required parameter.
                                 Example: pine task-to-task add -ci 1 .........
                                 Fool Example: pine task-to-task add -pi 1 -ci 2
                                 """)
    parser_.add_argument("-style", "--relation-style", choices=list(TASK_RELATIONS_TUPLE),
                         default=TaskRelation.CONNECTED.value,
                         help="""Relation style between this two tasks. 
                                 Blocking style - you can't execute parent task without executing subtask.
                                 Depend style - if all subtask are executed, task will be executed.
                                 Connected style - tasks connect only logically
                                 Example: pine task-to-task add -style 'blocking'
                                 """)


def _task_to_task_initialize_show_parser(parser_):
    parser_.add_argument("-i", "--relation-id", required=True, type=_positive_int,
                         help="""Id of task to task connection you want to show.
                                 This is a required parameter.
                                 Example: pine task-to-task show -i 1
                                 """)


def _task_to_task_initialize_delete_parser(parser_):
    parser_.add_argument("-pi", "--parent-id", required=True, type=_positive_int,
                         help="""Id of parent task, connection with which you want to delete.
                                 This is a required parameter.
                                 Example: pine task-to-task delete -pi 1 .........
                                 Fool Example: pine task-to-task delete -pi 1 -ci 2
                                 """)
    parser_.add_argument("-ci", "--child-id", required=True, type=_positive_int,
                         help="""Id of child task, connection with which you want to delete.
                                 This is a required parameter.
                                 Example: pine task-to-task delete -ci 1 .........
                                 Fool Example: pine task-to-task delete -pi 1 -ci 2
                                 """)


def _task_to_task_initialize_update_parser(parser_):
    parser_.add_argument("-i", "--relation-id", type=_positive_int, required=True,
                         help="""Id of task to task connection you want to update.
                                 This is a required parameter.
                                 Example: pine task-to-task update -i 1 .........
                                 Fool Example: pine task-to-task update -i 1 -style 'depends'
                                 """)
    parser_.add_argument("-style", "--relation-style", required=True, choices=list(TASK_RELATIONS_TUPLE),
                         help="""New relation style between this two tasks.
                                 This is a required parameter.
                                 Example: pine task-to-task add -style 'depends' .........
                                 Fool Example: pine task-to-task update -i 1 -style 'depends'
                                 """)


# endregion

# region Group to task connection parser: create and delete group-to-task parser
def _group_to_task_initialize_parser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions",
                                        description="""Group to task connections describes the 
                                        relationship between group and task. You can add and delete
                                        this connection.
                                        """)
    subparsers.required = True

    create_group_to_task_parser = subparsers.add_parser(commands.Commands.ADD,
                                                        help="Create group to task connections")
    _group_to_task_initialize_create_parser(create_group_to_task_parser)

    delete_group_to_task_parser = subparsers.add_parser(commands.Commands.DELETE,
                                                        help="Delete group to task connection")
    _group_to_task_initialize_delete_parser(delete_group_to_task_parser)


def _group_to_task_initialize_create_parser(parser_):
    parser_.add_argument("-gi", "--group-id", required=True, type=_positive_int,
                         help="""Id of group in which you want to add task.
                                 This is a required parameter.
                                 Example: pine group-to-task add -gi 1 .........
                                 Fool Example: pine group-to-task add -gi 1 -ti 2
                                 """)
    parser_.add_argument("-ti", "--task-id", required=True, type=_positive_int,
                         help="""Id of task, which you want to add in specified group.
                                 This is a required parameter.
                                 Example: pine group-to-task add -ti 1 .........
                                 Fool Example: pine group-to-task add -gi 1 -ti 2
                                 """)


def _group_to_task_initialize_delete_parser(parser_):
    parser_.add_argument("-gi", "--group-id", required=True, type=_positive_int,
                         help="""Id of group in which you want to delete task.
                                 This is a required parameter.
                                 Example: pine group-to-task delete -gi 1 .........
                                 Fool Example: pine group-to-task delete -gi 1 -ti 2
                                 """)
    parser_.add_argument("-ti", "--task-id", required=True, type=_positive_int,
                         help="""Id of task, which you want to delete in specified group.
                                 This is a required parameter.
                                 Example: pine group-to-task delete -ti 1 .........
                                 Fool Example: pine group-to-task delete -gi 1 -ti 2
                                 """)


# endregion

# region Executor to task: create and delete executor to task parser
def _executor_to_task_initialize_parser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions",
                                        description="""Executor to task connections describes the 
                                        relationship between executor and task. You can add and delete
                                        this connection.
                                        """)
    subparsers.required = True

    create_executor_to_task_parser = subparsers.add_parser(commands.Commands.ADD,
                                                           help="Create executor to task connections")
    _executor_to_task_initialize_create_parser(create_executor_to_task_parser)

    delete_executor_to_task_parser = subparsers.add_parser(commands.Commands.DELETE,
                                                           help="Delete executor to task connection")
    _executor_to_task_initialize_delete_parser(delete_executor_to_task_parser)


def _executor_to_task_initialize_create_parser(parser_):
    parser_.add_argument("-ei", "--executor-id", required=True, type=_positive_int,
                         help="""Id of executor to whom you want to entrust (add) task.
                                This is a required parameter.
                                Example: pine executor-to-task add -ei 1 .........
                                Fool Example: pine executor-to-task add -ei 1 -ti 2
                                """)
    parser_.add_argument("-ti", "--task-id", required=True, type=_positive_int,
                         help="""Id of task, which you want to entrust (add) to executor.
                                This is a required parameter.
                                Example: pine executor-to-task add -ti 1 .........
                                Fool Example: pine executor-to-task add -ei 1 -ti 2
                                """)


def _executor_to_task_initialize_delete_parser(parser_):
    parser_.add_argument("-ei", "--executor-id", required=True, type=_positive_int,
                         help="""Id of executor to whom you want to delete task.
                                This is a required parameter.
                                Example: pine executor-to-task delete -ei 1 .........
                                Fool Example: pine executor-to-task delete -ei 1 -ti 2
                                """)
    parser_.add_argument("-ti", "--task-id", required=True, type=_positive_int,
                         help="""Id of task, which you want to delete to executor.
                                This is a required parameter.
                                Example: pine executor-to-task delete -ti 1 .........
                                Fool Example: pine executor-to-task delete -ei 1 -ti 2
                                """)


# endregion
# endregion

# region Alert parser: initialize reminder subparser; create, show, delete and update alert;
def _alert_initialize_subparser(parser_):
    subparsers = parser_.add_subparsers(dest=commands.Commands.SECOND, title="Possible actions",
                                        description="""Alert will notify you of task regularly""")
    subparsers.required = True

    create_alert_parser = subparsers.add_parser(commands.Commands.ADD, formatter_class=MultilineFormatter,
                                                help="Create periodic alert for task")
    _alert_initialize_create_parser(create_alert_parser)

    show_alert_parser = subparsers.add_parser(commands.Commands.SHOW, formatter_class=MultilineFormatter,
                                              help="Show alert by id")
    _alert_initialize_show_parser(show_alert_parser)

    update_alert_parser = subparsers.add_parser(commands.Commands.UPDATE, formatter_class=MultilineFormatter,
                                                help="Update alert by id")
    _alert_initialize_update_parser(update_alert_parser)

    delete_alert_parser = subparsers.add_parser(commands.Commands.DELETE, formatter_class=MultilineFormatter,
                                                help="Delete alert by id")
    _alert_initialize_delete_parser(delete_alert_parser)


def _alert_initialize_create_parser(parser_):
    parser_.add_argument("-i", "--task-id", required=True, type=_positive_int,
                         help="""The alert will be added to the task selected by its id.
                                 This is a required parameter.
                                 Example: pine alert add -i 1 ........ 
                                 Fool Example: pine alert add -i 1 -start 23-08-2019 --time 1:30
                                 """)
    parser_.add_argument("-start", "--start-date", required=True, type=_future_date,
                         help="""The alert will remind you about you task since this date.
                                 This is a required parameter.
                                 Enter time in format DD-MM-YYYY.
                                 Example: pine alert add -i 1 -start 23-08-2018 ........ 
                                 Fool Example: pine alert add -i 1 -start 23-08-2019 --time 1:30
                                 """)
    parser_.add_argument("--days-interval", required=True, type=_positive_int,
                         help="""Interval in days with which you will be informed.
                                 This is a required parameter.
                                 Example: pine alert add --days-interval 2 ........
                                 Fool Example: pine alert add -i 1 -start 23-08-2019 --days-interval 2
                                 """)


def _alert_initialize_show_parser(parser_):
    parser_.add_argument("-i", "--alert-id", required=True, type=_positive_int,
                         help="""Id of alert you want to show.
                                 This is a required parameter.
                                 Example: pine alert show -i 1
                                 """)


def _alert_initialize_delete_parser(parser_):
    parser_.add_argument("-i", "--alert-id", required=True, type=_positive_int,
                         help="""Id of alert you want to delete.
                                 This is a required parameter.
                                 Example: pine alert delete -i 1
                                 """)


def _alert_initialize_update_parser(parser_):
    parser_.add_argument("-i", "--alert-id", required=True, type=_positive_int,
                         help="""Id of alert, you want to update.
                                 This is a required parameter.
                                 Example: pine alert update -i 1
                                 """)
    parser_.add_argument("-start", "--start-date", type=_future_date,
                         help="""The alert will remind you about you task since this date.
                                 Example: pine alert update -start 26-11-2019 ........ 
                                 Fool Example: pine alert update -i 1 -start 26-11-2019 
                                 """)
    parser_.add_argument("--days-interval", required=True, type=_positive_int,
                         help="""Interval in days with which you will be informed.
                                 This is a required parameter.
                                 Example: pine alert add --days-interval 2 ........
                                 Fool Example: pine alert add -i 1 -start 23-08-2019 --days-interval 2
                                 """)
    parser_.add_argument("-a", "--active", choices=[helpers.YES, helpers.NO],
                         help="""You can choose: activate (choice "yes") 
                                or deactivate (choice "no") alert
                                Example: pine alert update -a "yes" ........
                                Fool Example: pine alert update -i 1 -a "yes"
                                """)


# endregion

# region Initialization parser: create database
def _init_initialize_subparser(parser_):
    pass


# endregion

# region Login parser: create user and add him to database
def _login_initialize_subparser(parser_):
    parser_.add_argument("-u", "--username", required=True,
                         type=str,
                         help="""Your nickname.
                              This is a required parameter.
                              Example: pine login -u ligonberry
                              """)
# endregion
