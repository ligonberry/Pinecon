import datetime

import pytest

from pinecon.library.models import helpers
from pinecon.library.models.model.alert import Alert
from pinecon.library.models.model.executor_to_task import ExecutorToTaskRelation
from pinecon.library.models.model.group import Group
from pinecon.library.models.model.group_to_task import GroupToTaskRelation
from pinecon.library.models.model.message import Message
from pinecon.library.models.model.plan import Plan, PlanEndReason, PlanRepetitionStyle
from pinecon.library.models.model.task import Task, TaskStatus, TaskType, TaskPriority
from pinecon.library.models.model.task_date import TaskDate, TaskDateType
from pinecon.library.models.model.task_to_task import TaskToTaskRelation, TaskRelation
from pinecon.library.models.model.user import User

from pinecon.library.services.exceptions import NotFoundError
from pinecon.library.services.service import Service
from pinecon.library.storage.queries import Queries
from pinecon.library.storage.storage import Storage


@pytest.fixture(scope='module')
def setup_query():
    data_storage = Storage('sqlite:///test.db')
    query = Queries(data_storage)
    yield query
    data_storage.metadata.drop_all()


@pytest.fixture(scope='module')
def setup_service(setup_query):
    service = Service(setup_query)
    user = User(name='testUser')
    service.add_checked_user(user)
    user_id = user.user_id
    service = Service(setup_query, current_user_id=user_id)
    yield service
    setup_query.database.metadata.drop_all()


@pytest.fixture(scope='module')
def setup_service_without_user(setup_query):
    service = Service(setup_query)
    yield service
    setup_query.database.metadata.drop_all()


class TestTask:

    @pytest.fixture()
    def setup_task(self, setup_service):
        task = Task(title="Test", creator_id=1)
        added_task = setup_service.add_checked_task(task)
        yield added_task
        try:
            setup_service.delete_checked_task(added_task.task_id)
        except NotFoundError:
            pass

    def test_is_task_created(self, setup_service):
        task = Task(title="Test", creator_id=1)
        added_task = setup_service.add_checked_task(task)
        received_task = setup_service.get_checked_task_by_id(added_task.task_id)
        assert added_task.title == received_task.title
        assert added_task.task_id == received_task.task_id
        assert added_task.creator_id == received_task.creator_id
        assert added_task.status == received_task.status
        assert added_task.task_type == received_task.task_type
        assert added_task.priority == received_task.priority
        assert added_task.description == received_task.description
        assert added_task.subtask_dependency == received_task.subtask_dependency

    def test_is_task_received(self, setup_service, setup_task):
        received_task = setup_service.get_checked_task_by_id(setup_task.task_id)
        assert setup_task.title == received_task.title
        assert setup_task.task_id == received_task.task_id
        assert setup_task.creator_id == received_task.creator_id
        assert setup_task.status == received_task.status
        assert setup_task.task_type == received_task.task_type
        assert setup_task.priority == received_task.priority
        assert setup_task.description == received_task.description
        assert setup_task.subtask_dependency == received_task.subtask_dependency

    def test_is_task_deleted(self, setup_service, setup_task):
        setup_service.delete_checked_task(setup_task.task_id)
        with pytest.raises(NotFoundError):
            setup_service.get_checked_task_by_id(setup_task.task_id)

    def test_is_task_updated(self, setup_service, setup_task):
        updated_title = 'updatedTest'
        updated_status = TaskStatus.WORK.value
        updated_task_type = TaskType.PLAN.value
        updated_priority = TaskPriority.HIGH.value
        updated_description = 'updatedDescription'
        updated_subtask_dependency = helpers.YES
        updated_task = setup_service.update_checked_task(setup_task.task_id,
                                                         title=updated_title,
                                                         status=updated_status,
                                                         task_type=updated_task_type,
                                                         priority=updated_priority,
                                                         description=updated_description,
                                                         subtask_dependency=updated_subtask_dependency)
        received_task = setup_service.get_checked_task_by_id(updated_task.task_id)
        assert updated_title == received_task.title
        assert updated_status == received_task.status
        assert updated_task_type == received_task.task_type
        assert updated_priority == received_task.priority
        assert updated_description == received_task.description
        assert updated_subtask_dependency == received_task.subtask_dependency


class TestGroup:

    @pytest.fixture()
    def setup_group(self, setup_service):
        group = Group(title="TestGroup", creator_id=1)
        added_group = setup_service.add_checked_group(group)
        yield added_group
        try:
            setup_service.delete_checked_group_by_id(added_group.group_id)
        except NotFoundError:
            pass

    def test_is_group_created(self, setup_service):
        group = Group(title='TestGroup', creator_id=1)
        added_group = setup_service.add_checked_group(group)
        received_group = setup_service.get_checked_group_by_id(added_group.group_id)
        assert added_group.title == received_group.title
        assert added_group.group_id == received_group.group_id
        assert added_group.creator_id == received_group.creator_id
        assert added_group.group_type == received_group.group_type

    def test_is_group_received(self, setup_service, setup_group):
        received_group = setup_service.get_checked_group_by_id(setup_group.group_id)
        assert setup_group.title == received_group.title
        assert setup_group.group_id == received_group.group_id
        assert setup_group.creator_id == received_group.creator_id
        assert setup_group.group_type == received_group.group_type

    def test_is_group_deleted(self, setup_service, setup_group):
        setup_service.delete_checked_group_by_id(setup_group.group_id)
        with pytest.raises(NotFoundError):
            setup_service.get_checked_group_by_id(setup_group.group_id)

    def test_is_group_updated(self, setup_service, setup_group):
        updated_title = 'updatedTestGroup'
        updated_group = setup_service.update_checked_group(setup_group.group_id,
                                                           title=updated_title)
        received_group = setup_service.get_checked_group_by_id(updated_group.group_id)
        assert updated_title == received_group.title


class TestPlan:

    @pytest.fixture()
    def setup_plan(self, setup_service):
        task = Task(title="Test", creator_id=1)
        plan = Plan(start_date=datetime.datetime.strptime('01012080', '%d%m%Y').date(),
                    repeat_style=PlanRepetitionStyle.DAILY.value,
                    interval_counter=1,
                    end_reason=PlanEndReason.NEVER.value)
        added_task, added_plan = setup_service.add_checked_plan(template=task, plan=plan)
        yield added_plan
        try:
            setup_service.delete_checked_plan(added_plan.plan_id)
        except NotFoundError:
            pass

    def test_is_plan_created(self, setup_service):
        task = Task(title="Test", creator_id=1)
        plan = Plan(start_date=datetime.datetime.strptime('01012080', '%d%m%Y').date(),
                    repeat_style=PlanRepetitionStyle.DAILY.value,
                    interval_counter=1,
                    end_reason=PlanEndReason.NEVER.value)
        added_task, added_plan = setup_service.add_checked_plan(template=task,
                                                                plan=plan)
        received_plan = setup_service.get_checked_plan_by_id(added_plan.plan_id)
        assert added_plan.plan_id == received_plan.plan_id
        assert added_plan.task_id == received_plan.task_id
        assert added_plan.start_date == received_plan.start_date
        assert added_plan.repeat_style == received_plan.repeat_style
        assert added_plan.interval_counter == received_plan.interval_counter
        assert added_plan.end_reason == received_plan.end_reason
        assert added_plan.end_date == received_plan.end_date
        assert added_plan.count == received_plan.count
        assert added_plan.counter == received_plan.counter
        assert added_plan.active == received_plan.active

    def test_is_plan_received(self, setup_service, setup_plan):
        received_plan = setup_service.get_checked_plan_by_id(setup_plan.plan_id)
        assert setup_plan.plan_id == received_plan.plan_id
        assert setup_plan.task_id == received_plan.task_id
        assert setup_plan.start_date == received_plan.start_date
        assert setup_plan.repeat_style == received_plan.repeat_style
        assert setup_plan.interval_counter == received_plan.interval_counter
        assert setup_plan.end_reason == received_plan.end_reason
        assert setup_plan.end_date == received_plan.end_date
        assert setup_plan.count == received_plan.count
        assert setup_plan.counter == received_plan.counter
        assert setup_plan.active == received_plan.active

    def test_is_plan_deleted(self, setup_service, setup_plan):
        setup_service.delete_checked_plan(setup_plan.plan_id)
        with pytest.raises(NotFoundError):
            setup_service.get_checked_plan_by_id(setup_plan.plan_id)

    def test_is_plan_updated(self, setup_service, setup_plan):
        updated_start_date = datetime.datetime.strptime('01012090', '%d%m%Y').date()
        updated_repeat_style = PlanRepetitionStyle.MONTHLY.value
        updated_interval_counter = 2
        updated_end_reason = PlanEndReason.AFTER_NUMBER_OF_TIMES.value
        updated_end_date = datetime.datetime.strptime('01013000', '%d%m%Y').date()
        updated_count = 2
        updated_active = helpers.NO
        updated_plan = setup_service.update_checked_plan(setup_plan.plan_id,
                                                         start_date=updated_start_date,
                                                         repeat_style=updated_repeat_style,
                                                         interval_counter=updated_interval_counter,
                                                         end_reason=updated_end_reason,
                                                         end_date=updated_end_date,
                                                         count=updated_count,
                                                         active=updated_active)
        received_plan = setup_service.get_checked_plan_by_id(updated_plan.plan_id)
        assert updated_start_date == received_plan.start_date
        assert updated_repeat_style == received_plan.repeat_style
        assert updated_interval_counter == received_plan.interval_counter
        assert updated_end_reason == received_plan.end_reason
        assert updated_end_date == received_plan.end_date
        assert updated_count == received_plan.count
        assert updated_active == received_plan.active


class TestMessage:

    @pytest.fixture()
    def setup_message(self, setup_query):
        message = Message(title='TestMessage', user_id=1)
        added_message = setup_query.add_message(message)
        yield added_message
        try:
            setup_query.delete_message_by_id(added_message.message_id)
        except NotFoundError:
            pass

    def test_is_message_received(self, setup_service, setup_message):
        received_message = setup_service.get_checked_message_by_id(setup_message.message_id)
        assert setup_message.title == received_message.title
        assert setup_message.user_id == received_message.user_id
        assert setup_message.message_id == received_message.message_id
        assert setup_message.text == received_message.text
        assert setup_message.date == received_message.date
        assert setup_message.time == received_message.time

    def test_is_message_deleted(self, setup_service, setup_message):
        setup_service.delete_checked_message(setup_message.message_id)
        with pytest.raises(NotFoundError):
            setup_service.get_checked_message_by_id(setup_message.message_id)


class TestDate:

    @pytest.fixture()
    def setup_date(self, setup_service):
        task = Task(title="Test", creator_id=1)
        added_task = setup_service.add_checked_task(task)
        date = TaskDate(task_id=added_task.task_id, date_type=TaskDateType.EVENT.value,
                        date=datetime.datetime.strptime('01012080', '%d%m%Y').date())
        added_date = setup_service.add_checked_date(date)
        yield added_date
        try:
            setup_service.delete_checked_date_by_id(added_date.date_id)
        except NotFoundError:
            pass

    def test_is_date_created(self, setup_service):
        task = Task(title="Test", creator_id=1)
        added_task = setup_service.add_checked_task(task)
        date = TaskDate(task_id=added_task.task_id, date_type=TaskDateType.EVENT.value,
                        date=datetime.datetime.strptime('01012080', '%d%m%Y').date())
        added_date = setup_service.add_checked_date(date)
        received_date = setup_service.get_checked_date_by_id(added_date.date_id)
        assert added_date.date_id == received_date.date_id
        assert added_date.task_id == received_date.task_id
        assert added_date.date_type == received_date.date_type
        assert added_date.date == received_date.date

    def test_is_date_received(self, setup_service, setup_date):
        received_date = setup_service.get_checked_date_by_id(setup_date.date_id)
        assert setup_date.date_id == received_date.date_id
        assert setup_date.task_id == received_date.task_id
        assert setup_date.date_type == received_date.date_type
        assert setup_date.date == received_date.date

    def test_is_date_deleted(self, setup_service, setup_date):
        setup_service.delete_checked_date_by_id(setup_date.date_id)
        with pytest.raises(NotFoundError):
            setup_service.get_checked_date_by_id(setup_date.date_id)

    def test_is_date_updated(self, setup_service, setup_date):
        updated_date_type = TaskDateType.START.value
        updated_date_str = datetime.datetime.strptime('01012080', '%d%m%Y').date()
        updated_date = setup_service.update_checked_date(setup_date.date_id,
                                                         date_type=updated_date_type,
                                                         date=updated_date_str)
        received_date = setup_service.get_checked_date_by_id(updated_date.date_id)
        assert updated_date_type == received_date.date_type
        assert updated_date_str == received_date.date


class TestAlert:

    @pytest.fixture()
    def setup_alert(self, setup_service):
        task = Task(title="Test", creator_id=1)
        added_task = setup_service.add_checked_task(task)
        alert = Alert(task_id=added_task.task_id,
                      start_date=datetime.datetime.strptime('01012080', '%d%m%Y').date(),
                      date_interval='2:30')
        added_alert = setup_service.add_checked_alert(alert)
        yield added_alert
        try:
            setup_service.delete_checked_alert(added_alert.alert_id)
        except NotFoundError:
            pass

    def test_is_alert_created(self, setup_service):
        task = Task(title="Test", creator_id=1)
        added_task = setup_service.add_checked_task(task)
        alert = Alert(task_id=added_task.task_id,
                      start_date=datetime.datetime.strptime('01012080', '%d%m%Y').date(),
                      date_interval='2:30')
        added_alert = setup_service.add_checked_alert(alert)
        received_alert = setup_service.get_checked_alert_by_id(added_alert.alert_id)
        assert added_alert.alert_id == received_alert.alert_id
        assert added_alert.task_id == received_alert.task_id
        assert added_alert.start_date == received_alert.start_date
        assert added_alert.date_interval == received_alert.date_interval
        assert added_alert.active == received_alert.active

    def test_is_alert_received(self, setup_service, setup_alert):
        received_alert = setup_service.get_checked_alert_by_id(setup_alert.alert_id)
        assert setup_alert.alert_id == received_alert.alert_id
        assert setup_alert.task_id == received_alert.task_id
        assert setup_alert.start_date == received_alert.start_date
        assert setup_alert.date_interval == received_alert.date_interval
        assert setup_alert.active == received_alert.active

    def test_is_alert_deleted(self, setup_service, setup_alert):
        setup_service.delete_checked_alert(setup_alert.alert_id)
        with pytest.raises(NotFoundError):
            setup_service.get_checked_alert_by_id(setup_alert.alert_id)

    def test_is_alert_updated(self, setup_service, setup_alert):
        updated_start_date = datetime.datetime.strptime('01012090', '%d%m%Y').date()
        updated_date_interval = '3:10'
        updated_active = helpers.NO
        updated_alert = setup_service.update_checked_alert(alert_id=setup_alert.alert_id,
                                                           start_date=updated_start_date,
                                                           date_interval=updated_date_interval,
                                                           active=updated_active)
        received_alert = setup_service.get_checked_alert_by_id(updated_alert.alert_id)
        assert updated_start_date == received_alert.start_date
        assert updated_date_interval == received_alert.date_interval
        assert updated_active == received_alert.active


class TestTaskToTaskRelation:

    @pytest.fixture()
    def setup_task_to_task_relation(self, setup_service):
        parent_task = Task(title="TestParent", creator_id=1)
        child_task = Task(title="TestChild", creator_id=1)
        added_parent_task = setup_service.add_checked_task(parent_task)
        added_child_task = setup_service.add_checked_task(child_task)
        relation = TaskToTaskRelation(parent_task_id=added_parent_task.task_id,
                                      subtask_id=added_child_task.task_id)
        added_relation = setup_service.add_checked_task_to_task_relation(relation)
        yield added_relation
        try:
            setup_service.delete_checked_task_to_task_relation(added_parent_task.task_id,
                                                               added_child_task.task_id)
        except NotFoundError:
            pass

    def test_is_task_to_task_relation_created(self, setup_service):
        parent_task = Task(title="TestParent", creator_id=1)
        child_task = Task(title="TestChild", creator_id=1)
        added_parent_task = setup_service.add_checked_task(parent_task)
        added_child_task = setup_service.add_checked_task(child_task)
        relation = TaskToTaskRelation(parent_task_id=added_parent_task.task_id,
                                      subtask_id=added_child_task.task_id)
        added_relation = setup_service.add_checked_task_to_task_relation(relation)
        received_relation = setup_service.get_checked_task_to_task_relation_by_id(added_relation.relation_id)
        assert added_relation.parent_task_id == received_relation.parent_task_id
        assert added_relation.subtask_id == received_relation.subtask_id
        assert added_relation.relation_id == received_relation.relation_id
        assert added_relation.relation_type == received_relation.relation_type

    def test_is_task_to_task_relation_received(self, setup_service, setup_task_to_task_relation):
        received_relation = setup_service.get_checked_task_to_task_relation_by_id(setup_task_to_task_relation
                                                                                  .relation_id)
        assert setup_task_to_task_relation.parent_task_id == received_relation.parent_task_id
        assert setup_task_to_task_relation.subtask_id == received_relation.subtask_id
        assert setup_task_to_task_relation.relation_id == received_relation.relation_id
        assert setup_task_to_task_relation.relation_type == received_relation.relation_type

    def test_is_task_to_task_relation_deleted(self, setup_service, setup_task_to_task_relation):
        setup_service.delete_checked_task_to_task_relation(setup_task_to_task_relation.parent_task_id,
                                                           setup_task_to_task_relation.subtask_id)
        with pytest.raises(NotFoundError):
            setup_service.get_checked_task_to_task_relation_by_id(setup_task_to_task_relation.relation_id)

    def test_is_task_to_task_relation_updated(self, setup_service, setup_task_to_task_relation):
        updated_type = TaskRelation.DEPENDS_ON.value
        updated_relation = setup_service.update_checked_task_to_task_relation(setup_task_to_task_relation.relation_id,
                                                                              updated_type)
        received_relation = setup_service.get_checked_task_to_task_relation_by_id(updated_relation.relation_id)
        assert updated_type == received_relation.relation_type


class TestGroupToTaskRelation:

    @pytest.fixture()
    def setup_group_to_task_relation(self, setup_service):
        group = Group(title='TestGroup', creator_id=1)
        task = Task(title='Test', creator_id=1)
        added_task = setup_service.add_checked_task(task)
        added_group = setup_service.add_checked_group(group)
        relation = GroupToTaskRelation(task_id=added_task.task_id, group_id=added_group.group_id)
        added_relation = setup_service.add_checked_group_to_task_relation(relation)
        yield added_relation
        try:
            setup_service.delete_checked_group_to_task_relation(added_task.task_id, added_group.group_id)
        except NotFoundError:
            pass

    def test_is_group_to_task_relation_created(self, setup_query, setup_service):
        group = Group(title='TestGroup', creator_id=1)
        task = Task(title='Test', creator_id=1)
        added_task = setup_service.add_checked_task(task)
        added_group = setup_service.add_checked_group(group)
        relation = GroupToTaskRelation(task_id=added_task.task_id, group_id=added_group.group_id)
        added_relation = setup_service.add_checked_group_to_task_relation(relation)
        received_relation = setup_query.get_group_to_task_relation_by_id(added_relation.relation_id)
        assert added_relation.group_id == received_relation.group_id
        assert added_relation.task_id == received_relation.task_id
        assert added_relation.relation_id == received_relation.relation_id

    def test_is_group_to_task_relation_deleted(self, setup_query, setup_service, setup_group_to_task_relation):
        setup_service.delete_checked_group_to_task_relation(setup_group_to_task_relation.task_id,
                                                            setup_group_to_task_relation.group_id)
        received_relation = setup_query.get_group_to_task_relation_by_id(setup_group_to_task_relation.relation_id)
        assert received_relation is None


class TestExecutorToTaskRelation:

    @pytest.fixture()
    def setup_executor_to_task_relation(self, setup_service):
        executor = User(name='TestExecutor')
        task = Task(title='Test', creator_id=1)
        added_executor = setup_service.add_checked_user(executor)
        added_task = setup_service.add_checked_task(task)
        relation = ExecutorToTaskRelation(executor_id=added_executor.user_id,
                                          task_id=added_task.task_id)
        added_relation = setup_service.add_checked_executor_to_task_relation(relation)
        yield added_relation
        try:
            setup_service.delete_checked_executor_to_task_relation(added_task.task_id,
                                                                   added_executor.user_id)
        except NotFoundError:
            pass

    def test_is_executor_to_task_relation_created(self, setup_query, setup_service):
        executor = User(name='TestExecutor')
        task = Task(title='Test', creator_id=1)
        added_executor = setup_service.add_checked_user(executor)
        added_task = setup_service.add_checked_task(task)
        relation = ExecutorToTaskRelation(executor_id=added_executor.user_id,
                                          task_id=added_task.task_id)
        added_relation = setup_service.add_checked_executor_to_task_relation(relation)
        received_relation = setup_query.get_executor_to_task_relation_by_id(added_relation.relation_id)
        assert added_relation.executor_id == received_relation.executor_id
        assert added_relation.task_id == received_relation.task_id
        assert added_relation.relation_id == received_relation.relation_id
        assert added_relation.relation_confirmed == received_relation.relation_confirmed

    def test_is_executor_to_task_relation_deleted(self, setup_query, setup_service, setup_executor_to_task_relation):
        setup_service.delete_checked_executor_to_task_relation(setup_executor_to_task_relation.task_id,
                                                               setup_executor_to_task_relation.executor_id)
        received_relation = setup_query.get_executor_to_task_relation_by_id(setup_executor_to_task_relation.relation_id)
        assert received_relation is None


class TestUser:

    @pytest.fixture(scope='class')
    def setup_user_service(self, setup_query, setup_service_without_user):
        user = User(name='TestUserOwner')
        added_user = setup_service_without_user.add_checked_user(user)
        service = Service(setup_query, current_user_id=added_user.user_id)
        yield service
        try:
            setup_query.delete_user_by_id(added_user.user_id)
        except NotFoundError:
            pass
        setup_query.database.metadata.drop_all()

    @pytest.fixture()
    def setup_user(self, setup_query, setup_user_service):
        user = User(name='TestUserFirst')
        added_user = setup_user_service.add_checked_user(user)
        yield added_user
        try:
            setup_query.delete_user_by_id(added_user.user_id)
        except NotFoundError:
            pass

    def test_is_user_created(self, setup_user_service):
        user = User(name='TestUserSecond')
        added_user = setup_user_service.add_checked_user(user)
        received_user = setup_user_service.get_user_by_username('TestUserSecond')
        assert added_user.name == received_user.name
        assert added_user.user_id == received_user.user_id

    def test_is_user_received(self, setup_user_service, setup_user):
        received_user = setup_user_service.get_user_by_username('TestUserFirst')
        assert setup_user.name == received_user.name
        assert setup_user.user_id == received_user.user_id

    def test_is_owner_received(self, setup_user_service):
        owner = setup_user_service.get_user_by_username('TestUserOwner')
        received_user = setup_user_service.get_user()
        assert owner.name == received_user.name
        assert owner.user_id == received_user.user_id
