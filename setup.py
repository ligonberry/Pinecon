import setuptools

setuptools.setup(name='pinecon',
                 version='1.3',
                 description='''Memory like a sieve? Now there’s no need to remember all 
                                those things that you have to do, because Pinecon will do that for you! 
                                The application will help you to remind both about important events (birthdays, holidays) 
                                and about daily routine. You can create simple tasks and plans, group them 
                                and give them to other users for execution.''',
                 packages=setuptools.find_packages(),
                 install_requires=[
                     'python-dateutil',
                     'sqlalchemy',
                     'pytest'
                 ],
                 entry_points={
                     'console_scripts': ['pine=pinecon.main:main'],
                 },
                 include_package_data=True,
                 zip_safe=False)
